package command;

/**
 * Created with IntelliJ IDEA.
 * 类名：Television
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:31
 * 描述:命令模式接受者，电视机
 * 版本：V1.0
 */
public class Television {

    public void onTV(){
        System.out.println("电视机正在启动中......");
    }

    public void offTV(){
        System.out.println("电视机正在关闭中......");
    }

    public void volDown(){
        System.out.println("电视机音量减小一度......");
    }

    public void volUp(){
        System.out.println("电视机音量增加一度......");
    }

    public void channelUp(){
        System.out.println("电视频道增+1......");
    }

    public void channelDown(){
        System.out.println("电视机频道增加-1......");
    }



}
