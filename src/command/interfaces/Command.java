package command.interfaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：Command
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:36
 * 描述:抽象命令角色
 * 版本：V1.0
 */
public interface Command {

    void execute();
}
