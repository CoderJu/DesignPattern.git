package command;

import command.impl.NoCommand;
import command.interfaces.Command;

/**
 * Created with IntelliJ IDEA.
 * 类名：ControlPanel
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:42
 * 描述: 遥控器
 * 版本：V1.0
 */
public class ControlPanel {

    private static final int CONTROL_SIZE = 7;
    private Command[] commands;

    /**
     * 初始化所有按钮指向空的按钮，避免后面出现异常
     */
    public ControlPanel() {
        commands = new Command[CONTROL_SIZE];
        for (int i = 0; i <commands.length ; i++) {
            commands[i]=new NoCommand();
        }
    }

    /**
     * 把命令绑定到对应的键上
     * @param index
     * @param command
     */
    public void addCommand(int index ,Command command){
        commands[index] =  command;
    }

    /**
     * 模拟点击按钮
     * @param index
     */
    public void press(int index){
        commands[index].execute();
    }


}
