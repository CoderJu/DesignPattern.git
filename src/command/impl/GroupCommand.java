package command.impl;

import command.interfaces.Command;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 类名：GroupCommand
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 22:07
 * 描述:组合命令
 * 版本：V1.0
 */
public class GroupCommand  implements Command{

    private List<Command> commandList = new ArrayList<Command>();

    /**
     * 添加系列命令
     * @param command
     */
    public void addCommand(Command command){
        commandList.add(command);
    }

    /**
     * 移除命令
     * @param command
     */
    public  void removeCommand(Command command){
        commandList.remove(command);
    }

    @Override
    public void execute() {

        for (Command command:commandList
             ) {
            command.execute();
        }
    }
}
