package command.impl;

import command.interfaces.Command;

/**
 * Created with IntelliJ IDEA.
 * 类名：NoCommand
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:43
 * 描述:
 * 版本：V1.0
 */
public class NoCommand implements Command {


    public NoCommand() {
    }

    @Override
    public void execute() {
        System.out.println("该按钮暂时还未绑定命令！");
    }
}
