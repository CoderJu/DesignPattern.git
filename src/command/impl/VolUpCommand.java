package command.impl;

import command.Television;
import command.interfaces.Command;

/**
 * Created with IntelliJ IDEA.
 * 类名：ChannelDownCommand
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:39
 * 描述:
 * 版本：V1.0
 */
public class VolUpCommand implements Command {

    private Television television;

    public VolUpCommand(Television television) {
        this.television = television;
    }

    @Override
    public void execute() {
        television.volUp();
    }
}
