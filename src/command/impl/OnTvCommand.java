package command.impl;

import command.Television;
import command.interfaces.Command;

/**
 * Created with IntelliJ IDEA.
 * 类名：OnTvCommand
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:37
 * 描述:具体命令角色 打开电视机
 * 版本：V1.0
 */
public class OnTvCommand  implements Command{

    private Television television;

    public OnTvCommand(Television television) {
        this.television = television;
    }

    @Override
    public void execute() {
        television.onTV();
    }
}
