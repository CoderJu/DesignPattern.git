package command.impl;

import command.Television;
import command.interfaces.Command;

/**
 * Created with IntelliJ IDEA.
 * 类名：OffTvCommand
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:39
 * 描述:
 * 版本：V1.0
 */
public class OffTvCommand implements Command {

    //持有接收者对象
    private Television television;

    public OffTvCommand(Television television) {
        this.television = television;
    }

    @Override
    public void execute() {
        television.offTV();
    }
}
