package command;

import command.impl.*;
import command.interfaces.Command;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/13 21:49
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        //初始化遥控器
        ControlPanel controlPanel = new ControlPanel();

        //初始化接收者
        Television television = new Television();

        //初始化系类命令
        GroupCommand groupCommand = new GroupCommand();
        groupCommand.addCommand(new OnTvCommand(television));
        groupCommand.addCommand(new ChannelUpCommand(television));
        groupCommand.addCommand(new VolDownCommand(television));

        //为遥控器上的键设置命令
        controlPanel.addCommand(0,new ChannelDownCommand(television));
        controlPanel.addCommand(1,new ChannelUpCommand(television));
        controlPanel.addCommand(2,new OffTvCommand(television));
        controlPanel.addCommand(3,new OnTvCommand(television));
        controlPanel.addCommand(4,new VolDownCommand(television));
        controlPanel.addCommand(5,new VolUpCommand(television));
        controlPanel.addCommand(6,groupCommand);

        //点击按钮
        controlPanel.press(5);
        controlPanel.press(4);
        controlPanel.press(3);
        controlPanel.press(2);
        controlPanel.press(1);
        controlPanel.press(0);

        //执行系列命令
        System.out.println("==================分割线==================");
        controlPanel.press(6);


    }
}
