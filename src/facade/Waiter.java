package facade;

/**
 * Created with IntelliJ IDEA.
 * 类名：TaoBao
 * 开发人员: CoderJu
 * 创建时间: 2019/1/15 22:51
 * 描述:
 * 版本：V1.0
 */
public class Waiter {


    public  void order(){
        HongShaoRou hongShaoRou = HongShaoRou.getInstance();
        MaPoDouFu maPoDouFu = MaPoDouFu.getInstance();

        maPoDouFu.get();
        hongShaoRou.cook();
    }
}
