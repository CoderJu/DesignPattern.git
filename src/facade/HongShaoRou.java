package facade;

/**
 * Created with IntelliJ IDEA.
 * 类名：GoodShopMall
 * 开发人员: CoderJu
 * 创建时间: 2019/1/15 22:49
 * 描述:
 * 版本：V1.0
 */
public class HongShaoRou {

    private static HongShaoRou instance = null;

    private HongShaoRou() {
    }

    public   static HongShaoRou getInstance(){
        if (instance==null){
            instance = new HongShaoRou();
        }
        return instance;
    }

    public  void get(){
        System.out.println("去菜市场肉店店买原材料<<<<<<");
    }
    public  void wash(){
        System.out.println("清洗原材料<<<<<<");
    }
    public  void cook(){
        System.out.println("烹饪<<<<<<");
    }
}
