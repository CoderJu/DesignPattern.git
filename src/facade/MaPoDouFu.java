package facade;

/**
 * Created with IntelliJ IDEA.
 * 类名：BaiCaoWeiMall
 * 开发人员: CoderJu
 * 创建时间: 2019/1/15 22:50
 * 描述:
 * 版本：V1.0
 */
public class MaPoDouFu {

    private static  MaPoDouFu instance = null;

    private MaPoDouFu() {
    }
    public static MaPoDouFu getInstance(){
        if (instance==null){
            instance=new MaPoDouFu();
        }
        return instance;
    }

    public  void get(){
        System.out.println("去菜市场豆腐店买原材料<<<<<<");
    }
    public  void wash(){
        System.out.println("清洗原材料<<<<<<");
    }
    public  void cook(){
        System.out.println("烹饪<<<<<<");
    }

}
