package facade;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/15 23:01
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {
       Waiter waiter = new Waiter();
       waiter.order();

       MaPoDouFu maPoDouFu = MaPoDouFu.getInstance();
       maPoDouFu.get();

    }
}
