package mediator;

import mediator.impl.Airconditioning;
import mediator.impl.ConcreteMediator;
import mediator.impl.Door;
import mediator.impl.Light;
import mediator.intrefaces.Mediator;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:17
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {

        Mediator mediator = new ConcreteMediator();

        Door door = new Door(mediator,"123");
        Light light = new Light(mediator,"3333");
        Airconditioning airconditioning = new Airconditioning(mediator,"8888");

        door.openDoor();

        door.CloseDoor();

    }
}
