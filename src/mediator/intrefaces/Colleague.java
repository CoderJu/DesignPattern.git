package mediator.intrefaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：Colleague
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 22:57
 * 描述:
 * 版本：V1.0
 */
public abstract  class Colleague {


    private Mediator mediator;
    public String name;

    public Colleague(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }

    public Mediator getMediator() {
        return mediator;
    }

    public abstract void senMsg(int status);
}
