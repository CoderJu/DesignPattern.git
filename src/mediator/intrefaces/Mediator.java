package mediator.intrefaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：Mediator
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 22:55
 * 描述:
 * 版本：V1.0
 */
public  abstract class Mediator {

    public abstract void register(String colleagueName,Colleague colleague );

    public abstract void getMsg(int status,String colleagueName);

    public abstract void sendMsg();


}
