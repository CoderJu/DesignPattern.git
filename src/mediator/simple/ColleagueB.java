package mediator.simple;

import mediator.simple.interfaces.AbsColleague;
import mediator.simple.interfaces.AbsMediator;

/**
 * Created with IntelliJ IDEA.
 * 类名：ColleagueB
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:50
 * 描述:
 * 版本：V1.0
 */
public class ColleagueB extends AbsColleague {
    @Override
    public void setNumber(int number, AbsMediator mediator) {
        this.number=number;
        mediator.BeffectA();
    }
}
