package mediator.simple;

import mediator.simple.interfaces.AbsColleague;
import mediator.simple.interfaces.AbsMediator;

/**
 * Created with IntelliJ IDEA.
 * 类名：Mediator
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:48
 * 描述:
 * 版本：V1.0
 */
public class Mediator extends AbsMediator {
    public Mediator(AbsColleague colleagueA, AbsColleague colleagueB) {
        super(colleagueA, colleagueB);
    }
    @Override
    public void AeffectB() {
        int num = A.getNumber();
        B.setNumber(num*100);
    }
    @Override
    public void BeffectA() {
        int num = B.getNumber();
        A.setNumber(num/100);
    }
}
