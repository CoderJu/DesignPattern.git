package mediator.simple;

import mediator.simple.interfaces.AbsColleague;
import mediator.simple.interfaces.AbsMediator;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:50
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {

        AbsColleague absColleagueA = new ColleagueA();
        AbsColleague absColleagueB = new ColleagueB();

        AbsMediator absMediator = new Mediator(absColleagueA,absColleagueB);
        absColleagueA.setNumber(155,absMediator);
        absMediator.AeffectB();
        System.out.println(absColleagueB.getNumber());
    }
}
