package mediator.simple.interfaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：AbsColleague
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:42
 * 描述:
 * 版本：V1.0
 */
public abstract class AbsColleague {
    protected  int number;
    public int getNumber() {
        return number;
    }
    public void setNumber(int number) {
        this.number = number;
    }
    public abstract  void setNumber(int number, AbsMediator mediator);
}
