package mediator.simple.interfaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：AbsMediator
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:43
 * 描述:
 * 版本：V1.0
 */
public  abstract  class AbsMediator  {
    protected AbsColleague A;
    protected  AbsColleague B;
    public AbsMediator(AbsColleague colleagueA, AbsColleague colleagueB) {
          A = colleagueA;
          B = colleagueB;
    }
    public abstract void AeffectB();
    public abstract void  BeffectA();
}
