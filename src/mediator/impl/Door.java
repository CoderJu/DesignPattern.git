package mediator.impl;

import mediator.intrefaces.Colleague;
import mediator.intrefaces.Mediator;

/**
 * Created with IntelliJ IDEA.
 * 类名：Door
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:02
 * 描述:
 * 版本：V1.0
 */
public class Door extends Colleague {

    public Door(Mediator mediator, String name) {
        super(mediator, name);
        mediator.register(name,this);
    }


    public void openDoor(){
        System.out.println(name+"开门");
        senMsg(0);
    }

    public void CloseDoor( ){
        System.out.println("关门");
        senMsg(1);
    }

    @Override
    public void senMsg(int status) {
        this.getMediator().getMsg(status,name);
    }
}
