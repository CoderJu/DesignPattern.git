package mediator.impl;

import mediator.intrefaces.Colleague;
import mediator.intrefaces.Mediator;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * 类名：ConcreteMediator
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:10
 * 描述:
 * 版本：V1.0
 */
public class ConcreteMediator extends Mediator {

    private HashMap<String,Colleague>  colleagueHashMap;
    private HashMap<String,String> interMap;

    public ConcreteMediator() {
        colleagueHashMap= new HashMap<String,Colleague>();
        interMap = new HashMap<String,String>();
    }

    @Override
    public void register(String colleagueName, Colleague colleague) {
            colleagueHashMap.put(colleagueName,colleague);
            if (colleague instanceof Door){
                interMap.put("Door",colleagueName);
            }else if (colleague instanceof Light){
                interMap.put("Light",colleagueName);
            }else if (colleague instanceof Airconditioning){
                interMap.put("Airconditioning",colleagueName);
            }
    }

    @Override
    public void getMsg(int status, String colleagueName) {
        if (colleagueHashMap.get(colleagueName) instanceof  Door){
            if (status==0){
                ((Light)(colleagueHashMap.get(interMap.get("Light")))).onLight(status);
                ((Airconditioning)(colleagueHashMap.get(interMap.get("Airconditioning")))).onCondition(status);
            }else if (status==1){
                ((Light)(colleagueHashMap.get(interMap.get("Light")))).offLight(status);
                ((Airconditioning)(colleagueHashMap.get(interMap.get("Airconditioning")))).offCondition(status);
            }
        }else if (colleagueHashMap.get(colleagueName) instanceof Light) {


        } else if (colleagueHashMap.get(colleagueName) instanceof Airconditioning) {


        }
    }

    @Override
    public void sendMsg() {

    }
}
