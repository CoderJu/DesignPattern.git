package mediator.impl;

import mediator.intrefaces.Colleague;
import mediator.intrefaces.Mediator;

/**
 * Created with IntelliJ IDEA.
 * 类名：Airconditioning
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:08
 * 描述:
 * 版本：V1.0
 */
public class Airconditioning extends Colleague {

    public Airconditioning(Mediator mediator, String name) {
        super(mediator, name);
        mediator.register(name,this);
    }

    public void onCondition(int ariStatus){
        System.out.println("开启空调");
        //senMsg(ariStatus);
    }

    public void offCondition(int ariStatus){
        System.out.println("关闭空调");
        //senMsg(ariStatus);
    }

    @Override
    public void senMsg(int status) {
        this.getMediator().getMsg(status,this.name);
    }
}
