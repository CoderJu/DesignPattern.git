package mediator.impl;

import mediator.intrefaces.Colleague;
import mediator.intrefaces.Mediator;

/**
 * Created with IntelliJ IDEA.
 * 类名：Light
 * 开发人员: CoderJu
 * 创建时间: 2019/2/26 23:05
 * 描述:
 * 版本：V1.0
 */
public class Light extends Colleague {

    public Light(Mediator mediator, String name) {
        super(mediator, name);
        mediator.register(name,this);
    }

    public void onLight(int lightStatus){
        System.out.println("开灯");
       senMsg(lightStatus);
    }

    public void offLight(int lightStatus){
        System.out.println("关灯");
        senMsg(lightStatus);
    }

    @Override
    public void senMsg(int status) {
        this.getMediator().getMsg(status,this.name);
    }
}
