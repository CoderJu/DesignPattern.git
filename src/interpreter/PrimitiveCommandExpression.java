package interpreter;

/**
 * Created with IntelliJ IDEA.
 * 类名：PrimitiveCommandExpression
 * 开发人员: CoderJu
 * 创建时间: 2019/2/25 22:37
 * 描述:基本命令节点类：终结符表达式
 * 版本：V1.0
 */
public class PrimitiveCommandExpression extends Expression {
    private String name;
    private String textStr;

    @Override
    void interpreter(Context text) {
        name = text.currentToken();
        text.skipToken(name);
        if (!name.equals("PRINT") && !name.equals("BREAK") && !name.equals ("SPACE")){
            System.err.println("非法命令！");
        }
        if (name.equals("PRINT")){
            textStr = text.currentToken();
            text.nextToken();
        }
    }

    @Override
    public void execute() {
        if (name.equals("PRINT")){
            System.out.print(textStr);}
        else if (name.equals("SPACE")){
            System.out.print(" ");}
        else if (name.equals("BREAK")){
            System.out.println();}
    }
}
