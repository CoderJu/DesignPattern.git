package interpreter;

import java.util.StringTokenizer;

/**
 * Created with IntelliJ IDEA.
 * 类名：Context
 * 开发人员: CoderJu
 * 创建时间: 2019/2/25 22:19
 * 描述:环境类：用于存储和操作需要解释的语句，在本实例中每一个需要解释的单词可以称为一个动作标记(Action Token)或命令
 * 版本：V1.0
 */
public class Context {
    /**
     * 用于将字符串分解为更小的字符串标记(Token)，默认情况下以空格作为分隔符
     */
    private StringTokenizer stringTokenizer;
    /**
     * 当前字符串标记
     */
    private String currentToken;

    public Context(String text) {
        stringTokenizer = new StringTokenizer(text);
        nextToken();
    }

    /**
     * 返回下一个标记
     * @return
     */
    public String nextToken() {
        if (stringTokenizer.hasMoreTokens()){
            currentToken = stringTokenizer.nextToken();
        }else {
            currentToken = null;
        }
        return currentToken;
    }

    /**
     * 返回当前的标记
     * @return
     */
    public String currentToken() {
        return currentToken;
    }

    /**
     * 跳过一个标记
     * @param token
     */
    public void skipToken(String token){
        if (!token.equals(currentToken)) {
            System.err.println("错误提示：" + currentToken + "解释错误！");
        }
        nextToken();
    }

    /**
     * 如果当前的标记是一个数字，则返回对应的数值
     * @return
     */
    public int currentNumber(){
        int number = 0;
        try{
            number = Integer.parseInt(currentToken); //将字符串转换为整数
        }catch(NumberFormatException e) {
            System.err.println("错误提示：" + e);
        }
        return number;
    }
}
