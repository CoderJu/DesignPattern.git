package interpreter;

/**
 * Created with IntelliJ IDEA.
 * 类名：LoopCommandExpression
 * 开发人员: CoderJu
 * 创建时间: 2019/2/25 22:36
 * 描述:循环命令节点类：非终结符表达式
 * 版本：V1.0
 */
public class LoopCommandExpression extends Expression {

    private int number;
    private Expression commandExpression;

    @Override
    void interpreter(Context text) {
        text.skipToken("LOOP");
        number=text.currentNumber();
        text.nextToken();
        commandExpression = new ExpressionNode();
        commandExpression.interpreter(text);
    }

    @Override
    public void execute() {
        for(int i=0;i<number;i++) {
            commandExpression.execute();
        }
    }

}
