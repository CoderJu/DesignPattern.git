package interpreter;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：ExpressionNode
 * 开发人员: CoderJu
 * 创建时间: 2019/2/25 22:28
 * 描述:表达式节点类：非终结符表达式
 * 版本：V1.0
 */
public class ExpressionNode extends Expression {

    private ArrayList<Expression> list = new ArrayList<Expression>();
    @Override
    void interpreter(Context text) {
        while (true){
            if (text.currentToken() == null){
                break;
            }else if (text.currentToken().equals("END")) {
                text.skipToken("END");
                break;
            }else{
                Expression commandExpression = new CommandExpression();
                commandExpression.interpreter(text);
                list.add(commandExpression);
            }
        }
    }

    //循环执行命令集合中的每一条命令
    @Override
    public void execute() {
        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            ((Expression)iterator.next()).execute();
        }
    }
}
