package interpreter;

/**
 * Created with IntelliJ IDEA.
 * 类名：CommandEspression
 * 开发人员: CoderJu
 * 创建时间: 2019/2/25 22:31
 * 描述:语句命令节点类：非终结符表达式
 * 版本：V1.0
 */
public class CommandExpression extends Expression {
    private Expression expression;
    @Override
    void interpreter(Context text) {
        if (text.currentToken().equals("LOOP")){
            expression = new LoopCommandExpression();
            expression.interpreter(text);
        }else{
            expression = new PrimitiveCommandExpression();
            expression.interpreter(text);
        }
    }

    @Override
    public void execute() {
        expression.execute();
    }
}
