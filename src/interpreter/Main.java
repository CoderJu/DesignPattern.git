package interpreter;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/25 22:42
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args){
        String text = "LOOP 2 PRINT 杨过 SPACE SPACE PRINT 小龙女 BREAK END PRINT 郭靖 SPACE SPACE PRINT 黄蓉";
        Context context = new Context(text);

        Expression expression = new ExpressionNode();
        expression.interpreter(context);
        expression.execute();
    }
}
