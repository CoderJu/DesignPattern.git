package interpreter;

/**
 * Created with IntelliJ IDEA.
 * 类名：Expression
 * 开发人员: CoderJu
 * 创建时间: 2019/2/25 22:26
 * 描述:
 * 版本：V1.0
 */
public abstract class Expression {
    /**
     * 声明一个方法用于解释语句
     * @param text
     */
    abstract void interpreter(Context text);

    /**
     * 声明一个方法用于执行标记对应的命令
     */
    public abstract void execute();
}
