package singleton;

/**
 * Created with IntelliJ IDEA.
 * 类名：Singleton
 * 开发人员: CoderJu
 * 创建时间: 2019/1/10 20:59
 * 描述:静态内部类法
 * 版本：V1.0
 */
public class SingletonForClass {
    //构建静态内部类
    private static class SingleTonHolder {
        public static final SingletonForClass singletonForClass = new SingletonForClass();
    }
    //直接返回对象
    public static SingletonForClass getInstance(){
        return SingleTonHolder.singletonForClass;
    }
}
