package singleton;

/**
 * Created with IntelliJ IDEA.
 * 类名：Singleton
 * 开发人员: CoderJu
 * 创建时间: 2019/1/10 20:59
 * 描述:枚举实现单例
 * 版本：V1.0
 */
public enum  SingletonForEnum {

    SINGLEINSTANCE;

    private SingleObj singleObj;

    private SingletonForEnum() {

        singleObj = new SingleObj();
    }

    public SingleObj getInstance() {

        return singleObj;

    }

}
