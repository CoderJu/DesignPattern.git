package singleton;

/**
 * Created with IntelliJ IDEA.
 * 类名：Singleton
 * 开发人员: CoderJu
 * 创建时间: 2019/1/10 20:59
 * 描述:双检锁/双重校验锁
 * 版本：V1.0
 */
public class SingletonForDCL {

    //初始化SingletonFoHunger对象为空
    private volatile static SingletonForDCL singleton= null;

    //私有化构造方法
    private SingletonForDCL() {
    }

    //提供获取唯一对象的方法，当外部调用该方法时开始初始化
    public static SingletonForDCL getInstance(){
        //检查是否实例化
        if (singleton==null) {
            //线程安全的创建实例
            synchronized (SingletonForDCL.class) {
                //二次检查是否存在 不存在才正式创建实例
                if (singleton == null) {
                    singleton = new SingletonForDCL();
                }
            }
        }
        return singleton;
    }

}
