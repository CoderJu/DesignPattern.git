package singleton;

/**
 * Created with IntelliJ IDEA.
 * 类名：Singleton
 * 开发人员: CoderJu
 * 创建时间: 2019/1/10 20:59
 * 描述:饿汉式单例模式
 * 版本：V1.0
 */
public class SingletonForLazy {

    //创建 SingletonForLazy 的一个对象
    private static SingletonForLazy singleton= new SingletonForLazy();
    //私有化构造方法
    private SingletonForLazy() {
    }

    //提供获取唯一对象的方法
    public static SingletonForLazy getInstance(){
        return singleton;
    }
}
