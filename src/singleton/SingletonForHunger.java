package singleton;

/**
 * Created with IntelliJ IDEA.
 * 类名：Singleton
 * 开发人员: CoderJu
 * 创建时间: 2019/1/10 20:59
 * 描述:懒汉式单例模式
 * 版本：V1.0
 */
public class SingletonForHunger {

    //初始化SingletonFoHunger对象为空
    private static SingletonForHunger singleton= null;

    //私有化构造方法
    private SingletonForHunger() {
    }

    //提供获取唯一对象的方法，当外部调用该方法时开始初始化
    public static SingletonForHunger getInstance(){
        if (singleton==null) {
            singleton = new SingletonForHunger();
        }
        return singleton;
    }

}
