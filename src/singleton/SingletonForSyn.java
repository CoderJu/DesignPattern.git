package singleton;

/**
 * Created with IntelliJ IDEA.
 * 类名：Singleton
 * 开发人员: CoderJu
 * 创建时间: 2019/1/10 20:59
 * 描述:synchronized同步锁的单例模式处理多线程问题
 * 版本：V1.0
 */
public class SingletonForSyn {

    //初始化SingletonFoHunger对象为空
    private static SingletonForSyn singleton= null;

    //私有化构造方法
    private SingletonForSyn() {
    }

    //提供获取唯一对象的方法，当外部调用该方法时开始初始化
    public   static synchronized SingletonForSyn getInstance(){
        if (singleton==null) {
            singleton = new SingletonForSyn();
        }
        return singleton;
    }

}
