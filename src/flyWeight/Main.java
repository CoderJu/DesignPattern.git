package flyWeight;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/24 22:30
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {
       DataBaseConnPool dataBaseConnPool =   ConnPoolFactory.getPool("",0);
       dataBaseConnPool.use();

        DataBaseConnPool dataBaseConnPool1 =   ConnPoolFactory.getPool("",1);
        dataBaseConnPool1.use();

        DataBaseConnPool dataBaseConnPool2 =   ConnPoolFactory.getPool("",1);
        dataBaseConnPool2.use();

        System.out.println("本次运行创建新对象的数量为："+ConnPoolFactory.getCount());
    }
}
