package flyWeight;

/**
 * Created with IntelliJ IDEA.
 * 类名：DataConnPool
 * 开发人员: CoderJu
 * 创建时间: 2019/2/24 22:16
 * 描述:
 * 版本：V1.0
 */
public  interface DataBaseConnPool {
      void use();
}
