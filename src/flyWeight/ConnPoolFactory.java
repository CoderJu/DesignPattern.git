package flyWeight;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * 类名：ConnPoolFactory
 * 开发人员: CoderJu
 * 创建时间: 2019/2/24 22:23
 * 描述:
 * 版本：V1.0
 */
public class ConnPoolFactory {

    private static final HashMap<String,DataBaseConnPool> hashMap = new HashMap<>();

    public static DataBaseConnPool getPool(String name,int type){
        DataBaseConnPool dataBaseConnPool = hashMap.get(name);
        if (dataBaseConnPool == null){
            if (type == 0) {
                dataBaseConnPool = new SqlSeverConnPool();
                hashMap.put("sqlSever", dataBaseConnPool);
            }else {
                dataBaseConnPool = new MySqlConnPool();
                hashMap.put("MySql", dataBaseConnPool);
            }
        }
        return dataBaseConnPool;
    }

    //获得对象数量总数
     public static int getCount() {
         return hashMap.size();
     }
}
