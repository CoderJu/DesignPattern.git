package flyWeight;

/**
 * Created with IntelliJ IDEA.
 * 类名：SqlSeverConnPool
 * 开发人员: CoderJu
 * 创建时间: 2019/2/24 22:22
 * 描述:
 * 版本：V1.0
 */
public class SqlSeverConnPool implements DataBaseConnPool {
    @Override
    public void use() {
        System.out.println(">>>>>>>这是一个SqlSever数据库链接池。。。。。");
    }
}
