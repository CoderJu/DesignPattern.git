package chainms;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/21 21:52
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {

        Request request = new Request("中国联通",5000);
        Request request1 = new Request("中国移动",50000);
        Request request2 = new Request("sap",80000);
        Request request3 = new Request("weaver",150000);

        AbsHandle superiorHandle = new SuperiorHandle("Tom");
        AbsHandle managerHandle = new ManagerHandle("Jack");
        AbsHandle deputyDirectorHandle = new DeputyDirectorHandle("Mike");
        AbsHandle directorHandle = new DirectorHandle("Tony");

        //关联层级，形成链
        superiorHandle.setNextHandler(managerHandle);
        managerHandle.setNextHandler(deputyDirectorHandle);
        deputyDirectorHandle.setNextHandler(directorHandle);

        superiorHandle.handleRequest(request3);
        superiorHandle.handleRequest(request2);
        managerHandle.handleRequest(request1);
        superiorHandle.handleRequest(request);
    }
}
