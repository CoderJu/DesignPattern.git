package chainms;

/**
 * Created with IntelliJ IDEA.
 * 类名：ManagerHandle
 * 开发人员: CoderJu
 * 创建时间: 2019/2/21 21:48
 * 描述:经理审批
 * 版本：V1.0
 */
public class ManagerHandle  extends AbsHandle{


    public ManagerHandle(String leader) {
        super(leader + "Manager");
    }

    @Override
    void handleRequest(Request request) {
        float price = request.getOrderPrice();
        if (price > 10000  && price<=50000){
            System.out.println("金额10000以上50000以内，由经理审批！经理为："+this.leader);
        }else{
            getNextHandler().handleRequest(request);
        }
    }
}
