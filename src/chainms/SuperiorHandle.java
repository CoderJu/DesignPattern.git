package chainms;

/**
 * Created with IntelliJ IDEA.
 * 类名：superiorHandle
 * 开发人员: CoderJu
 * 创建时间: 2019/2/21 21:43
 * 描述:直接上级审批
 * 版本：V1.0
 */
public class SuperiorHandle extends AbsHandle {
    public SuperiorHandle(String leader) {
        super(leader +" ----superior");
    }
    @Override
    void handleRequest(Request request) {
        float price = request.getOrderPrice();
        if (price <= 10000){
            System.out.println("金额10000以内，由直接上级审批！直接上级为："+this.leader);
        }else{
            getNextHandler().handleRequest(request);
        }
    }
}
