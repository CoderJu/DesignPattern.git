package chainms;

/**
 * Created with IntelliJ IDEA.
 * 类名：DeputyDirectorHandle
 * 开发人员: CoderJu
 * 创建时间: 2019/2/21 21:50
 * 描述:副总监审批
 * 版本：V1.0
 */
public class DeputyDirectorHandle extends AbsHandle {

    public DeputyDirectorHandle(String leader) {
        super(leader+"-----DeputyDirector");
    }

    @Override
    void handleRequest(Request request) {
        float price = request.getOrderPrice();
        if (price > 50000  && price<=100000){
            System.out.println("金额50000以上100000以内，由副总监审批！副总监为："+this.leader);
        }else{
            getNextHandler().handleRequest(request);
        }
    }
}
