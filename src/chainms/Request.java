package chainms;

/**
 * Created with IntelliJ IDEA.
 * 类名：Request
 * 开发人员: CoderJu
 * 创建时间: 2019/2/21 21:39
 * 描述:请求
 * 版本：V1.0
 */
public class Request {

    private String customer;

    private float orderPrice;


    public Request(String customer, float orderPrice) {
        this.customer = customer;
        this.orderPrice = orderPrice;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public float getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(float orderPrice) {
        this.orderPrice = orderPrice;
    }
}
