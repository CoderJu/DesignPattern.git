package chainms;

/**
 * Created with IntelliJ IDEA.
 * 类名：AbsHandle
 * 开发人员: CoderJu
 * 创建时间: 2019/2/21 21:40
 * 描述:抽象处理者
 * 版本：V1.0
 */
public abstract class AbsHandle {

     String leader;

    AbsHandle nextHandler;

    public AbsHandle(String leader) {
        this.leader = leader;
    }

    abstract void handleRequest(Request request);

    public AbsHandle getNextHandler() {
        return nextHandler;
    }

    public void setNextHandler(AbsHandle nextHandler) {
        this.nextHandler = nextHandler;
    }
}
