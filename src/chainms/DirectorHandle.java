package chainms;

/**
 * Created with IntelliJ IDEA.
 * 类名：DirectorHandle
 * 开发人员: CoderJu
 * 创建时间: 2019/2/21 21:51
 * 描述:总监审批
 * 版本：V1.0
 */
public class DirectorHandle extends AbsHandle {

    public DirectorHandle(String leader) {
        super(leader+"----------------Director");
    }

    @Override
    void handleRequest(Request request) {
        float price = request.getOrderPrice();
        if (price > 100000  ){
            System.out.println("金额100000以上，由总监审批！总监为："+this.leader);
        }else{
            getNextHandler().handleRequest(request);
        }
    }
}
