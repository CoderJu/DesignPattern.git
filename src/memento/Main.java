package memento;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/27 23:06
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        CareTaker careTaker = new CareTaker();
        ChessPlayerOne chessPlayerOne = new ChessPlayerOne();
        ChessPlayTwo chessPlayTwo = new ChessPlayTwo();

        chessPlayerOne.stepOne();
        careTaker.createMemento("stepOne",chessPlayerOne.create());
        chessPlayerOne.show();
        chessPlayerOne.stepTwo();
        chessPlayerOne.show();
        careTaker.createMemento("stepTwo",chessPlayerOne.create());
        chessPlayerOne.read (careTaker.readMemento("stepOne"));
        chessPlayerOne.show();
        chessPlayerOne.stepThree();
        chessPlayerOne.show();


        chessPlayTwo.stepOne();
        careTaker.createMemento("stepOne",chessPlayTwo.createMemento());
        chessPlayTwo.show();
        chessPlayTwo.stepTwo();
        careTaker.createMemento("stepTwo",chessPlayTwo.createMemento());
        chessPlayTwo.show();
        chessPlayTwo.stepThree();
        careTaker.createMemento("stepThree",chessPlayTwo.createMemento());
        chessPlayerOne.show();
        chessPlayTwo.readMemento(careTaker.readMemento("stepTwo"));
        chessPlayTwo.show();


    }
}
