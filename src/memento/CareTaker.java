package memento;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * 类名：CareTaker
 * 开发人员: CoderJu
 * 创建时间: 2019/2/27 22:46
 * 描述:负责人角色
 * 版本：V1.0
 */
public class CareTaker {

    private HashMap<String,MementoIF> hashMap ;

    public CareTaker() {
        this.hashMap = new HashMap<>();
    }

    public HashMap<String, MementoIF> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<String, MementoIF> hashMap) {
        this.hashMap = hashMap;
    }

    /**
     * 创建备忘录
     * @param name
     * @param mementoIF
     */
    public void createMemento(String name,MementoIF mementoIF){
            this.hashMap.put(name,mementoIF);
    }

    /**
     * 读取备忘录存档
     * @param name
     * @return
     */
    public MementoIF readMemento(String name){
       return this.hashMap.get(name);
    }
}
