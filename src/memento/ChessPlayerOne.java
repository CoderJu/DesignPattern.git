package memento;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * 类名：ChessPlayer
 * 开发人员: CoderJu
 * 创建时间: 2019/2/27 22:52
 * 描述:
 * 版本：V1.0
 */
public class ChessPlayerOne {

    private HashMap<String,String >  status;

    public ChessPlayerOne() {
        this.status = new HashMap<>();
    }

    public MementoIF create(){
        return new Memento(status);
    }

    public void read(MementoIF mementoIF){
        status =   ((Memento)mementoIF).getStatus();
    }

    public void stepOne(){
        status.put("x","8");
        status.put("Y","10");
    }

    public void stepTwo(){
        status.put("x","18");
        status.put("Y","20");
    }

    public void stepThree(){
        status.put("x","88");
        status.put("Y","88");
    }

    public void show(){
        System.out.println(">>>>>>>>>>>>>>>>>>>"+status.toString());
    }


    /**
     * 备忘录角色
     */
    private class Memento implements MementoIF{
        private HashMap<String,String >  status;

        public Memento(HashMap<String, String> status) {
            this.status = new HashMap<>(status);
        }

        public HashMap<String, String> getStatus() {
            return status;
        }

        public void setStatus(HashMap<String, String> status) {
            this.status = status;
        }
    }
}
