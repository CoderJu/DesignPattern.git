package memento;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * 类名：ChessPlayTwo
 * 开发人员: CoderJu
 * 创建时间: 2019/2/27 23:39
 * 描述:
 * 版本：V1.0
 */
public class ChessPlayTwo {
    private ArrayList<String> status;

    public ChessPlayTwo() {
        status = new ArrayList<String>();
    }

    public MementoIF createMemento(){
        return new Memento(status);
    }

    public void readMemento(MementoIF mementoIF ){
            status = ((Memento)mementoIF).getStatus();
    }

    public void stepOne(){
        status.add("x:8;Y:18");
    }

    public void stepTwo(){
        status.add("x:18;Y:118");
    }

    public void stepThree(){
        status.add("x:118;Y:128");
    }

    public void show(){
        System.out.println(">>>>>>>>>>>>>>>>>>>"+status.toString());
    }



    private class Memento implements MementoIF{
        private ArrayList<String> status;


        public Memento(ArrayList<String> status) {
            this.status = new ArrayList<>(status);
        }


        private ArrayList<String> getStatus() {
            return status;
        }
    }
}
