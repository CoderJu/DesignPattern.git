package prototype;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * 类名：Address
 * 开发人员: CoderJu
 * 创建时间: 2019/2/28 21:32
 * 描述:
 * 版本：V1.0
 */
public class Address implements Serializable {
    private String email,addr,locat;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getLocat() {
        return locat;
    }

    public void setLocat(String locat) {
        this.locat = locat;
    }
}
