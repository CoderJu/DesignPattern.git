package prototype;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/28 21:29
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Customer customer = new Customer();
        Address address = new Address();
        customer.setAddress(address);
        Customer customer2 = (Customer) customer.deepClone();
        System.out.println("customer:"+customer+"||customer2:"+customer2);
        System.out.println("克隆的对象向是否相同："+customer.equals(customer2));
        System.out.println("克隆的地址向是否相同："+(customer.getAddress()).equals((customer2.getAddress())));
    }
}
