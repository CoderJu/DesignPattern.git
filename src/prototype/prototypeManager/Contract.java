package prototype.prototypeManager;

/**
 * Created with IntelliJ IDEA.
 * 类名：Contract
 * 开发人员: CoderJu
 * 创建时间: 2019/2/28 22:25
 * 描述:
 * 版本：V1.0
 */
public interface Contract  extends Cloneable{

   public Contract  clone();

   void display();
}
