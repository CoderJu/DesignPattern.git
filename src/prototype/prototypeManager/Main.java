package prototype.prototypeManager;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/2/28 22:34
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {
        ContractManager contractManager = ContractManager.getContractManager();
        Contract contract1,contract2,contract3,contract4;
        contract1 = contractManager.getContract("person");
        contract1.display();
        contract2 = contractManager.getContract("person");
        contract2.display();
        System.out.println(contract1.equals(contract2));

        contract3 = contractManager.getContract("legal");
        contract3.display();
        contract4 = contractManager.getContract("legal");
        contract4.display();
        System.out.println(contract3.equals(contract4));
    }
}
