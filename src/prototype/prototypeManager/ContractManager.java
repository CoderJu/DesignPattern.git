package prototype.prototypeManager;

import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 * 类名：ContractManger
 * 开发人员: CoderJu
 * 创建时间: 2019/2/28 22:30
 * 描述:
 * 版本：V1.0
 */
public class ContractManager {

    private Hashtable contractTable = new Hashtable();
    private static ContractManager contractManager = new ContractManager();

    private ContractManager() {
       contractTable.put("person",new PersonnelContract());
       contractTable.put("legal",new LegalContract());
    }

    public void addContract(String name,Contract contract){
        contractTable.put(name,contract);
    }

    public Contract getContract(String name){
       return ((Contract)contractTable.get(name)).clone();
    }

    public static ContractManager getContractManager() {
        return contractManager;
    }
}
