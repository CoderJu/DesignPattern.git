package prototype.prototypeManager;

/**
 * Created with IntelliJ IDEA.
 * 类名：LegalContract
 * 开发人员: CoderJu
 * 创建时间: 2019/2/28 22:27
 * 描述:
 * 版本：V1.0
 */
public class LegalContract implements Contract {
    @Override
    public Contract clone() {
        Contract legalContract  = null;
        try {
            legalContract = (Contract)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.out.println("不支持复制！");
        }
        return legalContract;
    }

    @Override
    public void display() {
        System.out.println("这是一份法务合同");
    }
}
