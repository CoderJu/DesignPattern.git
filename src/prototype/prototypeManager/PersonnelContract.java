package prototype.prototypeManager;

/**
 * Created with IntelliJ IDEA.
 * 类名：PersonnelContract
 * 开发人员: CoderJu
 * 创建时间: 2019/2/28 22:29
 * 描述:
 * 版本：V1.0
 */
public class PersonnelContract implements Contract {
    @Override
    public Contract clone() {
        Contract personnelContract = null;
        try {
            personnelContract = (Contract)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            System.out.println("不支持复制！");
        }
        return personnelContract;
    }

    @Override
    public void display() {
        System.out.println("这是人事合同");
    }
}
