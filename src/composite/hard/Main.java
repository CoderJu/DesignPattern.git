package composite.hard;



/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 23:24
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {
        Witress witress = new Witress();
        BreakfastMenu breakfastMenu = new BreakfastMenu();
        DinnerMenu dinnerMeun = new DinnerMenu();
        witress.addComponent(breakfastMenu);
        witress.addComponent(dinnerMeun);
        witress.print();

    }
}
