package composite.hard;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：Watress
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 23:20
 * 描述:
 * 版本：V1.0
 */
public class Witress {

    private ArrayList<MenuComponent> iterators = new ArrayList<MenuComponent>();

    public Witress() {

    }

    public void addComponent(MenuComponent menuComponent){
        iterators.add(menuComponent);
    }

    public void print(){
        Iterator iterator;
        MenuComponent menuComponent;
        for (int i = 0; i <iterators.size() ; i++) {
            iterators.get(i).print();
            iterator = (Iterator) iterators.get(i).getIterator();
            while (iterator.hasNext()){
                menuComponent = (MenuComponent) iterator.next();
                menuComponent.print();
            }
        }
    }
}
