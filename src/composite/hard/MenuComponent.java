package composite.hard;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：MenuComponent
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 22:18
 * 描述: 菜单和菜单超类
 * 版本：V1.0
 */
public abstract class MenuComponent {

    public String getName(){
        return "";
    }

    public String getDescription(){
        return "";
    }

    public float getPrice(){
        return 0;
    }

    public boolean isVegetable(){
        return false;
    }

    abstract void print();

    public Iterator getIterator(){
        return  new NullIterator();
    }
}
