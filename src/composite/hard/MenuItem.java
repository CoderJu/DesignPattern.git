package composite.hard;

/**
 * Created with IntelliJ IDEA.
 * 类名：MenuItem
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 22:34
 * 描述:菜单项
 * 版本：V1.0
 */
public class MenuItem extends MenuComponent {

    private String name,description;
    private float price;
    private boolean vegetable;

    public MenuItem(String name, String description, float price, boolean vegetable) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.vegetable = vegetable;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public float getPrice() {
        return price;
    }

    @Override
    public boolean isVegetable() {
        return vegetable;
    }
    @Override
    public void print() {
        System.out.println(getName()+":"+getDescription()+">>"+getPrice()+">>"+isVegetable());
    }
}
