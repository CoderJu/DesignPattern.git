package composite.hard;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：NullIterator
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 23:03
 * 描述:空迭代器
 * 版本：V1.0
 */
public class NullIterator implements Iterator {


    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Object next() {
        return null;
    }
}
