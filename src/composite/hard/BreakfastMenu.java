package composite.hard;


import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：BreakfastMenu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 22:57
 * 描述:
 * 版本：V1.0
 */
public class BreakfastMenu extends MenuComponent {
   private ArrayList<MenuComponent> menuItems;

    public BreakfastMenu() {
        menuItems =   new ArrayList<MenuComponent>();
        addItem("包子","面粉+肉馅",2,false);
        addItem("馒头","面粉 ",1,false);
        addItem("粽子","糯米+肉",8,false);
    }

    private void addItem(String name,String description,float price,boolean vegetable){
        MenuItem item = new MenuItem(name,description,price,vegetable);
        menuItems.add(item);
    }

    @Override
    public void print() {
        System.out.println(">>>>>>>>>>>>>>BreakfastMenu<<<<<<<<<<<<<<<");
    }

    @Override
    public Iterator getIterator() {
        return new ComposeIterator(menuItems.iterator());
    }
}
