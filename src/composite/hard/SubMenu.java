package composite.hard;

import java.util.Iterator;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * 类名：SubMenu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 22:40
 * 描述:子菜单项
 * 版本：V1.0
 */
public class SubMenu extends MenuComponent{

    private ArrayList<MenuComponent> menuItems;

    public SubMenu() {
        menuItems = new ArrayList<MenuComponent>();
        addItem("水果蛋糕","水果+蛋糕+奶油",150,false);
        addItem("巧克力蛋糕","巧克力+蛋糕+奶油",200,false);
        addItem("坚果蛋糕","坚果+蛋糕+奶油",160,false);
    }

    private void addItem(String name,String description,float price,boolean vegetable){
        MenuItem item = new MenuItem(name,description,price,vegetable);
        menuItems.add(item);
    }

    @Override
    public void print() {
        System.out.println(">>>>>>>>>>>>>>>>>>>>>>>SubMenu<<<<<<<<<<<<<<<<<<<<<<<<<");
    }

    @Override
    public Iterator getIterator() {
        return new ComposeIterator((Iterator) menuItems.iterator());
    }
}
