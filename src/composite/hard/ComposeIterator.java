package composite.hard;

import java.util.Iterator;

import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * 类名：ComposeIterator
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 23:09
 * 描述:符合迭代器类
 * 版本：V1.0
 */
public class ComposeIterator implements Iterator {

    private Stack<Iterator> stack = new Stack<Iterator>();

    public ComposeIterator(Iterator iterator) {
        stack.push(iterator);
    }

    @Override
    public boolean hasNext() {
        if (stack.empty()){
            return false;
        }
        Iterator iterator = stack.peek();
        if (!iterator.hasNext()){
            stack.pop();
            return hasNext();
        }else {
            return true;
        }
    }

    @Override
    public Object next() {
        if (hasNext()){
            Iterator iterator = stack.peek();
            MenuComponent menuComponent = (MenuComponent) iterator.next();
            stack.push(menuComponent.getIterator());
            return menuComponent;
        }
        return null;
    }


}
