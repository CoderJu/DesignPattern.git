package composite.hard;

import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：DinnerMenu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 22:47
 * 描述:
 * 版本：V1.0
 */
public class DinnerMenu extends MenuComponent {
    private final static int Max_Items = 5;
    private int numOfItems = 0;
    private MenuComponent[] menuComponents;

    public DinnerMenu() {
        menuComponents = new MenuComponent[Max_Items];
        addItem("麻婆豆腐","辣椒+豆腐",20,false);
        addItem("大白菜","白菜",18,true);
        addItem("青椒土豆丝","青椒+土豆丝",16,true);
        addItem("红烧肉","肉",40,false);
        addMenu(new SubMenu());
    }

    /**
     * 添加菜单项
     * @param name
     * @param description
     * @param price
     * @param vegetable
     */
    private void addItem(String name,String description,float price,boolean vegetable){
        MenuItem menuItem = new MenuItem(name,description,price,vegetable);
        if (numOfItems >= Max_Items){
            System.err.println("午餐菜单已满！");
        }else{
            menuComponents[numOfItems] = menuItem;
            numOfItems++;
        }

    }

    /**
     * 添加子菜单
     * @param menuComponent
     */
    public void addMenu(MenuComponent menuComponent){
        if (numOfItems >= Max_Items){
            System.err.println("午餐菜单已满！");
        }else{
            menuComponents[numOfItems] = menuComponent;
            numOfItems++;
        }

    }

    @Override
    void print() {
        System.out.println(">>>>>>>>>>>>>>DinnerMenu<<<<<<<<<<<<<<<");
    }

    @Override
    public Iterator getIterator() {
        return new ComposeIterator(new DinnerIterator());
    }

    /**
     * 由于数组没有迭代器因此需要自己创建
     */
    class DinnerIterator implements Iterator{

        private int position;

        public DinnerIterator() {
            position= 0;
        }

        @Override
        public boolean hasNext() {
            if (position<numOfItems){
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
           MenuComponent menuComponent = menuComponents[position];
           position++;
           return menuComponent;
        }


    }
}
