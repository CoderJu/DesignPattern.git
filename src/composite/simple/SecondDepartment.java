package composite.simple;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * 类名：SecondDepartment
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 21:39
 * 描述:叶子节点
 * 版本：V1.0
 */
public class SecondDepartment extends Department {

    private String name;

    public SecondDepartment(String name) {
        this.name = name;
    }

    @Override
    void add(Department department) {
        System.out.println("已经是叶子节点无法添加子节点！");
    }

    @Override
    void remove(Department department) {
        System.out.println("已经是叶子节点无法移除子节点！");
    }

    @Override
    ArrayList<Department> getChild() {
        System.out.println("已经是叶子节点无多余的子节点！");
        return null;
    }

    @Override
    void print() {
        System.out.println("叶子节点>>>>>>>>>>"+name);
    }
}
