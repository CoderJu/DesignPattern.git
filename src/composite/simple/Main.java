package composite.simple;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 21:52
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        ArrayList hr = new ArrayList();
        Department leaf = new SecondDepartment("人力资源业务一部");
        Department leaf1 = new SecondDepartment("人力资源业务2部");
        hr.add(leaf);
        hr.add(leaf1);
        Department departmentForHr = new FirstDepartment(hr,"人力资源部");
        departmentForHr.print();


        ArrayList pmo = new ArrayList();
        Department leaf2 = new SecondDepartment("PMO业务一部");
        Department leaf3 = new SecondDepartment("PMO业务一部");
        pmo.add(leaf2);
        pmo.add(leaf3);
        Department departmentForPMO = new FirstDepartment(pmo,"PMO");
        departmentForHr.print();


    }
}
