package composite.simple;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * 类名：Department
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 21:34
 * 描述:抽象根节点角色
 * 版本：V1.0
 */
public abstract class  Department {


    /**
     * 添加方法
     * @param department
     */
     abstract void add(Department department);

    /**
     * 移除方法
     * @param department
     */
     abstract void remove(Department department);

    /**
     * 获取子节点
     * @return
     */
     abstract ArrayList<Department> getChild();


    /**
     * 打印
     */
    abstract void print();
}
