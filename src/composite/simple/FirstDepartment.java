package composite.simple;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：FirstDepartment
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 21:43
 * 描述:
 * 版本：V1.0
 */
public class FirstDepartment extends Department {

    //用于存储叶子节点
    ArrayList<Department> departments = new ArrayList<Department>();
    private String name;

    public FirstDepartment(ArrayList<Department> departments,String name) {
        this.departments = departments;
        this.name = name;
    }

    @Override
    void add(Department department) {
              departments.add(department);
    }

    @Override
    void remove(Department department) {
          if (departments.contains(department)){
                  departments.remove(department);
           }else {
              System.out.println("不存在该部门，无法移除");
          }
    }

    @Override
     ArrayList<Department> getChild() {
           return departments;
    }

    @Override
    void print() {
        System.out.println("一级部门>>>>"+this.name);
           Iterator iterator =  departments.iterator();
           while (iterator.hasNext()){
               ((Department)iterator.next()).print();
           }
    }
}
