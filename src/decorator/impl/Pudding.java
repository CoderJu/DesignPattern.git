package decorator.impl;

import decorator.interfaces.Drink;
import decorator.interfaces.Supplement;

/**
 * Created with IntelliJ IDEA.
 * 类名：Pudding
 * 开发人员: CoderJu
 * 创建时间: 2019/1/8 23:23
 * 描述:
 * 版本：V1.0
 */
public class Pudding extends Supplement {

    public Pudding(Drink drink) {
        super(drink);
    }

    @Override
    public String name() {
        return super.name()+">>>>Add Pudding";
    }

    @Override
    public float price() {
        return super.price() + 3;
    }
}
