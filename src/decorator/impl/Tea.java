package decorator.impl;

import decorator.interfaces.Drink;

/**
 * Created with IntelliJ IDEA.
 * 类名：Tea
 * 开发人员: CoderJu
 * 创建时间: 2019/1/8 23:12
 * 描述:具体被装饰对象角色 茶
 * 版本：V1.0
 */
public class Tea implements Drink {

    @Override
    public String name() {
        return "only Tea";
    }

    @Override
    public float price() {
        return 10;
    }
}
