package decorator;

import decorator.impl.Milk;
import decorator.impl.Pudding;
import decorator.impl.Tea;
import decorator.interfaces.Drink;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/8 23:24
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {

        //初始化被装饰对象
        Drink tea = new Tea();

        //开始装饰
        tea = new Pudding(tea);
        tea = new Milk(tea);

        System.out.println("添加配料表："+tea.name()+"||最终价格："+tea.price() );
    }
}
