package decorator.java;

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * 类名：Mian
 * 开发人员: CoderJu
 * 创建时间: 2019/1/8 23:57
 * 描述:
 * 版本：V1.0
 */
public class Mian {

    public static void main(String[] args) throws IOException {
        int c;
        InputStream io = new UpperCaseInputStream(new BufferedInputStream(new FileInputStream("G:\\test.txt")));
        while ((c=io.read())>=0){
            System.out.print((char)c);
        }
    }
}
