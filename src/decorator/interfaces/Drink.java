package decorator.interfaces;/**
 * Created by User on 2019/1/8.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Drink
 * 开发人员: CoderJu
 * 创建时间: 2019/1/8 23:10
 * 描述:被装饰对象的抽象角色
 * 版本：V1.0
 */
public interface Drink {

    String name();

    float price();
}
