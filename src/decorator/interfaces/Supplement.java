package decorator.interfaces;/**
 * Created by User on 2019/1/8.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Supplement
 * 开发人员: CoderJu
 * 创建时间: 2019/1/8 23:16
 * 描述:抽象装饰者角色
 * 版本：V1.0
 */
public abstract class Supplement implements Drink {

    //传入一个被装饰对象
    private Drink drink;

    public Supplement(Drink drink) {
        this.drink = drink;
    }

    @Override
    public String name(){
        return drink.name();
    }

    @Override
    public float price() {
        return drink.price();
    }
}
