package observer.interfaces;/**
 * Created by User on 2019/1/6.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Subject
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 20:41
 * 描述:观察者模式中的被观察者
 * 版本：V1.0
 */
public interface Subject {

    //注册观察者
    void reigsterObserver(Observer o);

    //移除观察者
    void removeObserver(Observer o );

    //通知所有观察者
    void notifyAllObserver();

    //拉模型
    void notifyAllObserverByPull();
}
