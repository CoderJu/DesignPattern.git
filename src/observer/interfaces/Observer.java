package observer.interfaces;/**
 * Created by User on 2019/1/6.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Observer
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 20:42
 * 描述:观察者模式中的观察者接口
 * 版本：V1.0
 */
public interface Observer {

    //显示信息(被观察者主动推送的信息)
    void show(String info);

    //主动拉数据
    void showByPull(Subject subject);
}
