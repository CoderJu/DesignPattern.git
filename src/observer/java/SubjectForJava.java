package observer.java;

import java.util.Observable;

/**
 * Created with IntelliJ IDEA.
 * 类名：SubjectForJava
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 22:28
 * 描述:Java内置观察者
 * 版本：V1.0
 */
public class SubjectForJava extends Observable {

    private String info;

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setWork(String info){
        this.info=info;
        setChanged();
        notifyObservers();
        //notifyObservers(this);
    }
}
