package observer.java;

import java.util.Observable;
import java.util.Observer;

/**
 * Created with IntelliJ IDEA.
 * 类名：ObserverForJava
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 22:31
 * 描述:
 * 版本：V1.0
 */
public class ObserverForJava implements Observer {



    @Override
    public void update(Observable o, Object arg) {
        SubjectForJava subjectForJava = (SubjectForJava) o;
        System.out.println(">>>>>>>"+subjectForJava.getInfo());
    }
}
