package observer.java;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 22:33
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        SubjectForJava subjectForJava = new SubjectForJava();
        ObserverForJava observerForJava = new ObserverForJava();
        subjectForJava.addObserver(observerForJava);
        subjectForJava.setWork("你好！JAVA!");
    }
}
