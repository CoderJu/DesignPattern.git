package observer;

import observer.impl.User;
import observer.impl.WechatSever;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 20:54
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {
        //初始化微信公众号
        WechatSever wechatSever = new WechatSever();

        //初始化订阅用户
        User user = new User("ZhangSan");
        User user_1 = new User("LiSi");
        User user_2 = new User("WangWu");

        //注册用户为观察者
        wechatSever.reigsterObserver(user);
        wechatSever.reigsterObserver(user_1);
        wechatSever.reigsterObserver(user_2);

        //推送微信消息
       // wechatSever.sendInfo("今天是2019年一月6号，天气小雨，注意保暖");

        //ZhangSan取消订阅
        wechatSever.removeObserver(user);

        //再次推送
        //wechatSever.sendInfo("再次推送消息");


        //拉数据
        wechatSever.sendInfoByPull();

    }
}
