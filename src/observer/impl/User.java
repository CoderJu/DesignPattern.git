package observer.impl;

import observer.interfaces.Observer;
import observer.interfaces.Subject;

/**
 * Created with IntelliJ IDEA.
 * 类名：User
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 20:48
 * 描述:观察者模式中的观察者
 * 版本：V1.0
 */
public class User implements Observer{


    private String username;

    public User(String username) {
        this.username = username;
    }

    @Override
    public void show(String info) {
        System.out.println(username+":"+info);
    }

    //拉数据
    @Override
    public void showByPull(Subject subject) {
        String info = ((WechatSever)subject).getInfo();
        String href =  ((WechatSever)subject).getHref();
        System.out.println("Pull>>>>>"+username+"："+href);
    }
}
