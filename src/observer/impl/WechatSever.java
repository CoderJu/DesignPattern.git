package observer.impl;

import observer.interfaces.Observer;
import observer.interfaces.Subject;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * 类名：Wechat
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 20:45
 * 描述:观察模式中的微信公众号
 * 版本：V1.0
 */
public class WechatSever implements Subject {

    private ArrayList<Observer> users = new ArrayList<Observer>();
    private String info;
    private String href;

    @Override
    public void reigsterObserver(Observer o) {
        users.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        if (users.contains(o)){
            users.remove(o);
        }else{
            System.out.println("订阅者不存在");
        }
    }

    @Override
    public void notifyAllObserver() {
        for (Observer user: users ) {
                user.show(info);
        }
    }



    public void sendInfo(String s){
        info = s;
        notifyAllObserver();
    }

    //拉模型
    @Override
    public void notifyAllObserverByPull() {
        for (Observer user: users ) {
            user.showByPull(this);
        }
    }
    public void sendInfoByPull(){
        this.setInfo("JAVA Is Very Nice!!!!");
        this.setHref("www.baidu.com");
        notifyAllObserverByPull();
    }


    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
