package proxy.dynamicProxy;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 16:10
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        //目标对象
        Caculate caculate = new CaculateImpl();
        System.out.println(caculate.getClass());

        Caculate caculate1 = (Caculate) new ProxyFactory(caculate).getProxyInstance();
        System.out.println(caculate1.getClass());

        int result = 0;
        result = caculate1.add(1,2);
        System.out.println(">>>>>>>>>>>>>>>>>"+result);
    }
}
