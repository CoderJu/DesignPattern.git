package proxy.dynamicProxy;/**
 * Created by User on 2019/1/27.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Caculate
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 16:04
 * 描述:
 * 版本：V1.0
 */
public interface Caculate {
    int add(int a,int b) ;

    int sub(int a,int b) ;

    int mul(int a,int b) ;

    int div(int a,int b) ;
}
