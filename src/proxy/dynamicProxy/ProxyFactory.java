package proxy.dynamicProxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created with IntelliJ IDEA.
 * 类名：ProxyFactory
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 16:07
 * 描述:
 * 版本：V1.0
 */
public class ProxyFactory {

    //目标对象
    private Object target;

    public ProxyFactory(Object target) {
        this.target = target;
    }

    public Object getProxyInstance(){
        return  Proxy.newProxyInstance(target.getClass().getClassLoader(), target.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                //在转调具体目标对象之前，可以执行一些功能处理
                print();
                System.out.println(">>>>>>>>start"+"||"+method.getName());
                //执行目标方法
                Object returnValue  =   method.invoke(target,args);
                //在转调具体目标对象之前，可以执行一些功能处理
                System.out.println(">>>>>>>>end");
                return returnValue;
            }
        });
    }

    public void print(){
        System.out.println(">>>>>>>>>>You are not alone!");
    }
}
