package proxy.remoteProxy.service;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created with IntelliJ IDEA.
 * 类名：CaculateImpl
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 11:04
 * 描述:
 * 版本：V1.0
 */
@SuppressWarnings("serial")
public class CaculateImpl extends UnicastRemoteObject implements Caculate  {

    protected CaculateImpl() throws RemoteException {
    }

    @Override
    public int add(int a, int b) throws RemoteException {
        return a+b;
    }

    @Override
    public int sub(int a, int b) throws RemoteException {
        return a-b;
    }

    @Override
    public int mul(int a, int b) throws RemoteException {
        return a*b;
    }

    @Override
    public int div(int a, int b) throws RemoteException {
        return a/b;
    }


}
