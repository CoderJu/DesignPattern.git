package proxy.remoteProxy.service;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * Created with IntelliJ IDEA.
 * 类名：ServiceMain
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 11:07
 * 描述:
 * 版本：V1.0
 */
public class ServiceMain {
    public static void main(
            String[] args) {
        try {
            // 创建远程服务对象
            Caculate caculate = new CaculateImpl();
            // 绑定远程服务对象到 rmiregistry
            LocateRegistry.createRegistry(8088);
            Naming.rebind("rmi://127.0.0.1:8088/CalculatorService", caculate);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
