package proxy.remoteProxy.service;/**
 * Created by User on 2019/1/27.
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created with IntelliJ IDEA.
 * 类名：Caculate
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 11:01
 * 描述:
 * 版本：V1.0
 */
public interface Caculate extends Remote {

    int add(int a,int b) throws RemoteException;

    int sub(int a,int b) throws RemoteException;

    int mul(int a,int b) throws RemoteException;

    int div(int a,int b) throws RemoteException;
}
