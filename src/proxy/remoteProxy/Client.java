package proxy.remoteProxy;

import proxy.remoteProxy.service.Caculate;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created with IntelliJ IDEA.
 * 类名：Client
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 11:09
 * 描述:
 * 版本：V1.0
 */
public class Client {
    public static void main(String[] args) {
        try {
            Caculate caculate = (Caculate)
                    Naming.lookup( "rmi://127.0.0.1:8088/CalculatorService");
            int result = 0;
            result = caculate.add(1,2);
            System.out.println("add------"+result);
            result = caculate.div(5,1);
            System.out.println("div------"+result);
            result = caculate.mul(3,3);
            System.out.println("mul------"+result);
            result = caculate.sub(8,4);
            System.out.println("sub------"+result);
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
