package proxy.staticProxy;

/**
 * Created with IntelliJ IDEA.
 * 类名：CaculateImpl
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 15:35
 * 描述:
 * 版本：V1.0
 */
public class CaculateImpl implements Caculate {
    @Override
    public int add(int a, int b)  {
        return a+b;
    }

    @Override
    public int sub(int a, int b) {
        return a-b;
    }

    @Override
    public int mul(int a, int b) {
        return a*b;
    }

    @Override
    public int div(int a, int b) {
        return a/b;
    }


}
