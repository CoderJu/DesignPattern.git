package proxy.staticProxy;

/**
 * Created with IntelliJ IDEA.
 * 类名：Mian
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 15:37
 * 描述:
 * 版本：V1.0
 */
public class Mian {

    public static void main(String[] args) {
        CaculateImpl caculate = new CaculateImpl();

        CaculateProxy caculateProxy = new CaculateProxy(caculate);

        int result = 0;
        result = caculateProxy.add(1,3);
        System.out.println("add------------"+result);
    }
}
