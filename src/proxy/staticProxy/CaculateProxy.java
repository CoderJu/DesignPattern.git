package proxy.staticProxy;

/**
 * Created with IntelliJ IDEA.
 * 类名：CaculateProxy
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 15:35
 * 描述:
 * 版本：V1.0
 */
public class CaculateProxy implements Caculate {
    private  Caculate caculate;

    public CaculateProxy(Caculate caculate) {
        this.caculate = caculate;
    }

    @Override
    public int add(int a, int b) {
        return caculate.add(a,b);
    }

    @Override
    public int sub(int a, int b) {
        return caculate.sub(a,b);
    }

    @Override
    public int mul(int a, int b) {
        return caculate.mul(a,b);
    }

    @Override
    public int div(int a, int b) {
        return caculate.div(a,b);
    }
}
