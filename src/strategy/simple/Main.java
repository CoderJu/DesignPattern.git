package strategy.simple;

import strategy.simple.impl.OperationAdd;
import strategy.simple.impl.OperationMul;
import strategy.simple.impl.OperationSub;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 15:43
 * 描述:测试主函数
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        //测试Add
        Context context = new Context(new OperationAdd());
        System.out.println("Add:"+context.execute(1,5));
        //测试Mul
        context = new Context(new OperationMul());
        System.out.println("Mul:"+context.execute(8,3));
        //测试sub
        context = new Context(new OperationSub());
        System.out.println("Sub:"+context.execute(8,5));
    }
}
