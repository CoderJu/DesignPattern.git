package strategy.simple.impl;

import strategy.simple.interfaces.Operation;

/**
 * Created with IntelliJ IDEA.
 * 类名：OperationMul
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 15:40
 * 描述:
 * 版本：V1.0
 */
public class OperationMul implements Operation {
    @Override
    public int doOperate(int a, int b) {
       return a*b;
    }
}
