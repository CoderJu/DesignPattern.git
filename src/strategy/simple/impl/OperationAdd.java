package strategy.simple.impl;

import strategy.simple.interfaces.Operation;

/**
 * Created with IntelliJ IDEA.
 * 类名：OperationAdd
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 15:35
 * 描述:算法实现类,实现接口中的方法
 * 版本：V1.0
 */
public class OperationAdd implements Operation{

    @Override
    public int doOperate(int a, int b) {
        return a+b;
    }
}
