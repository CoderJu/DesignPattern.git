package strategy.simple;

import strategy.simple.interfaces.Operation;

/**
 * Created with IntelliJ IDEA.
 * 类名：context
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 15:40
 * 描述:内容类，用于调用不同的算法
 * 版本：V1.0
 */
public class Context {

    private Operation operation;

    public Context(Operation operation) {
        this.operation = operation;
    }

    public int execute(int a ,int b){
       return operation.doOperate(a,b);
    }
}
