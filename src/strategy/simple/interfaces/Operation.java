package strategy.simple.interfaces;/**
 * Created by User on 2019/1/6.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Operation
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 15:31
 * 描述:策略模式的策略角色
 * 版本：V1.0
 */
public interface Operation {
    int doOperate(int a,int b);
}
