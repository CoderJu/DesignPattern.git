package strategy.hard.impl;

import strategy.hard.interfaces.Price;

/**
 * Created with IntelliJ IDEA.
 * 类名：MiddleMember
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 16:02
 * 描述:中级会员
 * 版本：V1.0
 */
public class MiddleMember implements Price {
    @Override
    public double calPrice(double price) {
        return price*0.8;
    }
}
