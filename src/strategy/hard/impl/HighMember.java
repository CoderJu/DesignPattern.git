package strategy.hard.impl;

import strategy.hard.interfaces.Price;

/**
 * Created with IntelliJ IDEA.
 * 类名：HighMember
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 16:02
 * 描述:高级会员
 * 版本：V1.0
 */
public class HighMember implements Price {
    @Override
    public double calPrice(double price) {
        return price*0.5;
    }
}
