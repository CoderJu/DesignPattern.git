package strategy.hard.impl;

import strategy.hard.interfaces.Price;

/**
 * Created with IntelliJ IDEA.
 * 类名：NoMember
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 16:00
 * 描述:非会员直接返回原价
 * 版本：V1.0
 */
public class NoMember implements Price{
    @Override
    public double calPrice(double price) {
        return price;
    }
}
