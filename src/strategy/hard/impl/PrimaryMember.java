package strategy.hard.impl;

import strategy.hard.interfaces.Price;

/**
 * Created with IntelliJ IDEA.
 * 类名：PrimaryMember
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 16:01
 * 描述:初级会员
 * 版本：V1.0
 */
public class PrimaryMember implements Price {
    @Override
    public double calPrice(double price) {
        return price*0.9;
    }
}
