package strategy.hard;

import strategy.hard.interfaces.Price;

/**
 * Created with IntelliJ IDEA.
 * 类名：Member
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 16:03
 * 描述:会员体系
 * 版本：V1.0
 */
public class Member {
    private Price price;

    public Member(Price price) {
        this.price = price;
    }

    public double caculate(float oldPrice){
        return price.calPrice(oldPrice);
    }
}
