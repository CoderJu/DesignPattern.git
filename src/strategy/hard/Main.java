package strategy.hard;

import strategy.hard.impl.HighMember;
import strategy.hard.impl.NoMember;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 16:09
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        //高级会员
        Member member = new Member(new HighMember());
        System.out.println("高级会员："+member.caculate(100));
        //非会员
        member=new Member(new NoMember());
        System.out.println("非会员："+member.caculate(100));
    }
}
