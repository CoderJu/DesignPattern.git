package strategy.hard.interfaces;/**
 * Created by User on 2019/1/6.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Price
 * 开发人员: CoderJu
 * 创建时间: 2019/1/6 15:57
 * 描述:
 * 版本：V1.0
 */
public interface Price {
    double calPrice(double price);
}
