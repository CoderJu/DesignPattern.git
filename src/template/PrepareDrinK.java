package template;

/**
 * Created with IntelliJ IDEA.
 * 类名：PrepareDrin
 * 开发人员: CoderJu
 * 创建时间: 2019/1/16 21:56
 * 描述:抽象模板角色
 * 版本：V1.0
 */
public  abstract class PrepareDrinK {

    private void boilWater(){
        System.out.println("烧水了。。。。。");
    }

    public abstract void  brew();

    private void  pourInCup(){
        System.out.println("倒入茶杯中.....");
    }

    public abstract void drink();

    /**
     * 钩子方法
     * @return
     */
    public boolean doCondition(){
        return false;
    }

    public abstract void addCondition();

    //抽象模板模式的关键，一定要用关键字final
    public final void prepareDrink(){
        boilWater();
        brew();
        pourInCup();
        if (doCondition()){
            addCondition();
        }else {
            System.out.println("不需要添加其他东西");
        }
    }

}
