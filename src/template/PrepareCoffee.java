package template;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * 类名：PrepareCoffee
 * 开发人员: CoderJu
 * 创建时间: 2019/1/16 22:07
 * 描述:
 * 版本：V1.0
 */
public class PrepareCoffee extends PrepareDrinK {
    @Override
    public void brew() {
        System.out.println("放入咖啡粉。。。");
    }

    @Override
    public void drink() {
        System.out.println("喝咖啡。。。。。");
    }

    @Override
    public void addCondition() {
        System.out.println("加牛奶。。。。");
    }

    @Override
    public boolean doCondition() {

        System.out.println("您是否需要添加牛奶(y/n)：");
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String result = "";
        try {
            result = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (result.equals("n")){
            return false;
        }
        return  true;
    }
}
