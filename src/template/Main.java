package template;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/16 22:13
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        PrepareDrinK tea = new PrepareTea();
        tea.prepareDrink();

        PrepareDrinK coffee = new PrepareCoffee();
        coffee.prepareDrink();
    }
}
