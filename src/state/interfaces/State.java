package state.interfaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：State
 * 开发人员: CoderJu
 * 创建时间: 2019/1/26 22:21
 * 描述:
 * 版本：V1.0
 */
public interface State {



    /**
     * 预定
     */
    void book();

    /**
     * 退订
     */
    void Unsubscribe();

    /**
     * 吃饭
     */
    void eat();

    /**
     * 付钱走人
     */
    void payMoneyAndLeave();
}
