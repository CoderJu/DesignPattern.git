package state.impl;

import state.Table;
import state.interfaces.State;

/**
 * Created with IntelliJ IDEA.
 * 类名：hasMoneyState
 * 开发人员: CoderJu
 * 创建时间: 2019/1/26 22:31
 * 描述:已预订状态
 * 版本：V1.0
 */
public class BookedState implements State{


    private Table table;


    public BookedState(Table table) {
        this.table = table;
    }

    @Override
    public void book() {
        System.out.println("该餐桌已经给预定了...");
    }

    @Override
    public void Unsubscribe() {
        System.out.println("您已经成功取消预订了...");
        table.setState(table.freeState);
    }

    @Override
    public void eat() {
        System.out.println("您已经到达餐厅吃饭...");
        table.setState(table.occupiedState);
    }

    @Override
    public void payMoneyAndLeave() {

    }
}
