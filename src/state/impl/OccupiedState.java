package state.impl;

import state.Table;
import state.interfaces.State;

/**
 * Created with IntelliJ IDEA.
 * 类名：PlayState
 * 开发人员: CoderJu
 * 创建时间: 2019/1/26 22:31
 * 描述:已占用
 * 版本：V1.0
 */
public class OccupiedState implements State {
    private Table table;

    public OccupiedState(Table table) {
        this.table = table;
    }

    @Override
    public void book() {
        System.out.println("该餐桌已经在使用中...");
    }

    @Override
    public void Unsubscribe() {
        System.out.println("您已经成功取消了预订了...");
        table.setState(table.freeState);
    }

    @Override
    public void eat() {
        System.out.println("该餐桌已经在就餐中...");
    }

    @Override
    public void payMoneyAndLeave() {
        System.out.println("您已经付完钱离开...");
        table.setState(table.freeState);
    }
}
