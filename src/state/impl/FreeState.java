package state.impl;

import state.Table;
import state.interfaces.State;

/**
 * Created with IntelliJ IDEA.
 * 类名：ReadyState
 * 开发人员: CoderJu
 * 创建时间: 2019/1/26 22:24
 * 描述:
 * 版本：V1.0
 */
public class FreeState implements State {

    private Table table;

    public FreeState(Table table) {
        this.table = table;
    }

    @Override
    public void book() {
        System.out.println("您已经成功预订了...");
        table.setState(table.bookedState);
    }

    @Override
    public void Unsubscribe() {

    }

    @Override
    public void eat() {
        System.out.println("您已经成功开始等菜...");
        table.setState(table.occupiedState);
    }

    @Override
    public void payMoneyAndLeave() {
        try {
            throw  new Exception("空闲餐桌，无法付款");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
