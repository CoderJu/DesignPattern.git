package state;

import state.impl.BookedState;
import state.impl.FreeState;
import state.impl.OccupiedState;
import state.interfaces.State;

/**
 * Created with IntelliJ IDEA.
 * 类名：UFOCatcher
 * 开发人员: CoderJu
 * 创建时间: 2019/1/26 22:24
 * 描述:娃娃机
 * 版本：V1.0
 */
public class Table {

    public State bookedState;
    public State freeState;
    public State occupiedState;
    private State state ;

    public Table() {
        bookedState = new BookedState(this);
        freeState = new FreeState(this);
        occupiedState = new OccupiedState(this);
        state = freeState;//初始为空闲
    }



    public void setState(State state) {
        this.state = state;
    }

    /**
     * 预定
     */
    public  void book(){
        state.book();
    }

    /**
     * 退订
     */
    public void Unsubscribe(){
        state.Unsubscribe();
    }

    /**
     * 吃饭
     */
    public void eat(){
        state.eat();
    }

    /**
     * 付钱走人
     */
    public void payMoneyAndLeave(){
        state.payMoneyAndLeave();
    }


}
