package state;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/26 22:48
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
       Table table =  new Table();
       table.book();
       table.eat();
       table.payMoneyAndLeave();
       System.out.println("----------------------------------");

       table.payMoneyAndLeave();
    }
}
