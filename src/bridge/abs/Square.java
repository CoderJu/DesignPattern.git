package bridge.abs;

import bridge.interfaces.Color;

/**
 * Created with IntelliJ IDEA.
 * 类名：Square
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 23:27
 * 描述:
 * 版本：V1.0
 */
public class Square extends Shape {
    private Color color;

    public Square(Color color) {
        super(color);
       this.color = color;
    }

    @Override
    public void drowShape() {
        System.out.println("我是正方形。。。。");
        color.addColor();
    }
}
