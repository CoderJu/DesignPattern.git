package bridge.abs;

import bridge.interfaces.Color;

/**
 * Created with IntelliJ IDEA.
 * 类名：Circle
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 23:25
 * 描述:
 * 版本：V1.0
 */
    public class Circle extends Shape {

        private Color color;
        public Circle(Color color) {
            super(color);
            this.color = color;

        }

        @Override
        public void drowShape() {
            System.out.println("我画的事圆形......");
            color.addColor();
        }
    }
