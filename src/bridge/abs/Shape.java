package bridge.abs;

import bridge.interfaces.Color;

/**
 * Created with IntelliJ IDEA.
 * 类名：Shape
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 23:24
 * 描述:
 * 版本：V1.0
 */
public abstract class Shape {

    private Color color;

    public Shape(Color color) {
        this.color = color;
    }

   public abstract void drowShape();
}
