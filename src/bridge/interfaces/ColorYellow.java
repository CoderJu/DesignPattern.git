package bridge.interfaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：ColorYellow
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 23:21
 * 描述:
 * 版本：V1.0
 */
public class ColorYellow implements Color {
    @Override
    public void addColor() {
        System.out.println("我来涂黄色.......");
    }
}
