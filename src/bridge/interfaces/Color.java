package bridge.interfaces;/**
 * Created by User on 2019/1/27.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Color
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 23:16
 * 描述:
 * 版本：V1.0
 */
public interface Color {

    void addColor();
}
