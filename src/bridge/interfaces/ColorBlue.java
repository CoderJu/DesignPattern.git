package bridge.interfaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：ColorBlue
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 23:22
 * 描述:
 * 版本：V1.0
 */
public class ColorBlue implements Color {


    @Override
    public void addColor() {
        System.out.println("我要涂蓝色.....");
    }


}
