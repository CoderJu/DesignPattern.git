package bridge;

import bridge.abs.Circle;
import bridge.abs.Shape;
import bridge.abs.Square;
import bridge.interfaces.Color;
import bridge.interfaces.ColorBlue;
import bridge.interfaces.ColorRed;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/27 23:28
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        Color color;
        Shape shape;

        color = new ColorRed();
        shape = new Circle(color);
        shape.drowShape();

        color = new ColorBlue();
        shape = new Square(color);
        shape.drowShape();

    }
}
