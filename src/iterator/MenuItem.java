package iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：MenuItem
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 21:45
 * 描述:菜单项
 * 版本：V1.0
 */
public class MenuItem {

    private String name;

    private float price;

    public MenuItem(String name, float price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
