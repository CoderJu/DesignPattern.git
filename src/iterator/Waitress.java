package iterator;

import iterator.impl.BreakFastMeun;
import iterator.impl.DinnerMeun;
import iterator.interfaces.Iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 类名：Waitress
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 22:12
 * 描述:
 * 版本：V1.0
 */
public class Waitress {

    public static void main(String[] args) {
        BreakFastMeun breakFastMeun = new BreakFastMeun();
        DinnerMeun dinnerMeun = new DinnerMeun();

        Iterator iteratorForB =  breakFastMeun.createIterator();
        Iterator iteratorForD = dinnerMeun.createIterator();

        List<Iterator> iterators = new ArrayList<Iterator>();
        iterators.add(iteratorForB);
        iterators.add(iteratorForD);

        for (int i = 0; i <iterators.size() ; i++) {
            while (iterators.get(i).hasNext()){
                System.out.println(iterators.get(i).next().toString());
            }

        }
    }

}
