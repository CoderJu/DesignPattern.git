package iterator.interfaces;/**
 * Created by User on 2019/1/21.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Aggregate
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 21:50
 * 描述:
 * 版本：V1.0
 */
public interface Aggregate {

    Iterator createIterator();
}
