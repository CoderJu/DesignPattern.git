package iterator.interfaces;/**
 * Created by User on 2019/1/21.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Iterator
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 21:43
 * 描述: 迭代器接口
 * 版本：V1.0
 */
public interface Iterator {

        boolean hasNext();

        Object next();

        void remove(Object item);
}
