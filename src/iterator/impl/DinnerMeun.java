package iterator.impl;

import iterator.MenuItem;
import iterator.interfaces.Aggregate;
import iterator.interfaces.Iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 类名：BreakFastDinner
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 22:11
 * 描述:
 * 版本：V1.0
 */
public class DinnerMeun implements Aggregate {

    private List list;

    public DinnerMeun() {
        list = new ArrayList();
        add(new MenuItem("麻婆豆腐",18f));
        add(new MenuItem("土豆丝",12f));
        add(new MenuItem("红烧肉",30f));
    }

    public void add(MenuItem menuItem){
        list.add(menuItem);
    }

    @Override
    public Iterator createIterator() {
        return new DinnerIterator(list);
    }


}
