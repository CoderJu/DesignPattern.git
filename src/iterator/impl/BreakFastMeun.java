package iterator.impl;

import iterator.MenuItem;
import iterator.interfaces.Aggregate;
import iterator.interfaces.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：BreakFastMeun
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 22:10
 * 描述:
 * 版本：V1.0
 */
public class BreakFastMeun implements Aggregate {

    private MenuItem[] menuItems ;
    private int num=0;

    public BreakFastMeun() {

        menuItems = new MenuItem[3];
       addMeunItem(new MenuItem("包子",1.5f));
       addMeunItem(new MenuItem("馒头",1f));
       addMeunItem(new MenuItem("粽子",5f));
    }

    @Override
    public Iterator createIterator() {
        return new BreakfastIterator(menuItems);
    }

    public void addMeunItem(MenuItem menuItem){

        if (num >= menuItems.length) {
            System.err.println("sorry,menu is full!can not add another item");
        } else {
            menuItems[num] = menuItem;
            num++;
        }
    }
}
