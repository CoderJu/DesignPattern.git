package iterator.impl;

import iterator.interfaces.Iterator;

/**
 * Created with IntelliJ IDEA.
 * 类名：BreakfastIterator
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 21:54
 * 描述:
 * 版本：V1.0
 */
public class BreakfastIterator implements Iterator {

    private Object[] items = new Object[5];
    private int position;

    public BreakfastIterator(Object[] items) {
        this.items = items;
        this.position = 0;
    }

    @Override
    public boolean hasNext() {
        if (position<items.length){
            return true;
        }
        return false;
    }

    @Override
    public Object next() {
        Object obj = items[position];
        if (position>5){
            throw new IndexOutOfBoundsException("菜单只有五项!");
        }else{
            position++;
        }
        return obj;

    }

    @Override
    public void remove(Object item) {
        Object[] objects = new Object[5];
        for (int i = 0; i <items.length ; i++) {
                if (!items[i].equals(item)){
                    objects[i] = items[i];
                }

        }
        items = objects;
    }
}
