package iterator.impl;

import iterator.interfaces.Iterator;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 类名：DinnerIterator
 * 开发人员: CoderJu
 * 创建时间: 2019/1/21 21:53
 * 描述:
 * 版本：V1.0
 */
public class DinnerIterator implements Iterator {

    private List<Object> list;
    private int position ;

    public DinnerIterator(List<Object> list) {
        this.list = list;
        this.position = 0;
    }

    @Override
    public boolean hasNext() {
        if (position < list.size()){
            return true;
        }
        return false;
    }

    @Override
    public Object next() {
        Object menuItem = list.get(position);
        position++;
        return menuItem;
    }

    @Override
    public void remove(Object item) {
        list.remove(item);
    }
}
