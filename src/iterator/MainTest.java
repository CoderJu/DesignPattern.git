package iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * 类名：MainTest
 * 开发人员: CoderJu
 * 创建时间: 2019/1/23 20:21
 * 描述:
 * 版本：V1.0
 */
public class MainTest {

    public static void main(String[] args) {
        List list = new ArrayList();
        list.add("123");
        list.add("456");
        list.add("789");

        Iterator iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
