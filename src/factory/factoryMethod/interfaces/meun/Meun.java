package factory.factoryMethod.interfaces.meun;/**
 * Created by User on 2019/1/12.
 */

import factory.factoryMethod.interfaces.food.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：Meun
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 17:09
 * 描述:
 * 版本：V1.0
 */
public interface Meun {

    Food  createFood(String type);

    Food  createFood();
}
