package factory.factoryMethod.impl.food;

import factory.factoryMethod.interfaces.food.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：MapoTofuForOther
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 17:07
 * 描述:
 * 版本：V1.0
 */
public class MapoTofuForOther implements Food{

    @Override
    public void prepare() {
        System.out.println("MapoTofuForOther>>>>>准备中ing");
    }

    @Override
    public void cook() {
        System.out.println("MapoTofuForOther>>>>>烹饪中ing");
    }

    @Override
    public void serve() {
        System.out.println("MapoTofuForOther >>>>>上菜ing");
    }

    @Override
    public void order() {
        prepare();
        cook();
        serve();
    }
}
