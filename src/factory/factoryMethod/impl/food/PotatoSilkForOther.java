package factory.factoryMethod.impl.food;

import factory.factoryMethod.interfaces.food.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：PotatoSilkForOther
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 17:07
 * 描述:
 * 版本：V1.0
 */
public class PotatoSilkForOther implements Food {

    @Override
    public void prepare() {
        System.out.println("PotatoSilkForOther>>>>>准备中ing");
    }

    @Override
    public void cook() {
        System.out.println("PotatoSilkForOther>>>>>烹饪中ing");
    }

    @Override
    public void serve() {
        System.out.println("PotatoSilkForOther>>>>>上菜ing");
    }

    @Override
    public void order() {
        prepare();
        cook();
        serve();

    }
}
