package factory.factoryMethod.impl.food;

import factory.factoryMethod.interfaces.food.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：MapoTofuForChinese
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 17:07
 * 描述:
 * 版本：V1.0
 */
public class MapoTofuForChinese implements Food {
    @Override
    public void prepare() {
        System.out.println("麻婆豆腐>>>>>准备中ing");
    }

    @Override
    public void cook() {
        System.out.println("麻婆豆腐>>>>>烹饪中ing");
    }

    @Override
    public void serve() {
        System.out.println("麻婆豆腐 >>>>>上菜ing");
    }

    @Override
    public void order() {
        prepare();
        cook();
        serve();
    }
}
