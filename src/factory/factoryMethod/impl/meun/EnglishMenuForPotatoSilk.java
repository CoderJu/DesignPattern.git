package factory.factoryMethod.impl.meun;

import factory.factoryMethod.impl.food.MapoTofuForOther;
import factory.factoryMethod.impl.food.PotatoSilkForOther;
import factory.factoryMethod.interfaces.food.Food;
import factory.factoryMethod.interfaces.meun.Meun;

/**
 * Created with IntelliJ IDEA.
 * 类名：EnglishMenu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 17:15
 * 描述:
 * 版本：V1.0
 */
public class EnglishMenuForPotatoSilk implements Meun {
    @Override
    public Food createFood(String type) {
        return null;
    }

    @Override
    public Food createFood() {
        return new PotatoSilkForOther();
    }

}
