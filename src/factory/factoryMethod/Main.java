package factory.factoryMethod;

import factory.factoryMethod.impl.meun.ChineseMenu;
import factory.factoryMethod.impl.meun.ChineseMenuForMapoDofu;
import factory.factoryMethod.impl.meun.EnglishMenu;
import factory.factoryMethod.impl.meun.EnglishMenuForPotatoSilk;
import factory.factoryMethod.interfaces.food.Food;
import factory.factoryMethod.interfaces.meun.Meun;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 17:16
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        Meun meun = null;
        Food food = null;
        meun = new ChineseMenuForMapoDofu();
        food = meun.createFood();
        food.order();
        meun = new EnglishMenuForPotatoSilk();
        food = meun.createFood();
        food.order();
    }
}
