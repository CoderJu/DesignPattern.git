package factory.simplefactory.impl;

import factory.simplefactory.interfaces.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：MapoTofu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 15:30
 * 描述:
 * 版本：V1.0
 */
public class MapoTofu implements Food {
    @Override
    public void prepare() {
        System.out.println("MapoTofu>>>>>准备中ing");
    }

    @Override
    public void cook() {
        System.out.println("MapoTofu>>>>>烹饪中ing");
    }

    @Override
    public void serve() {
        System.out.println("MapoTofu>>>>>上菜ing");
    }

    @Override
    public void order() {
        prepare();
        cook();
        serve();
    }
}
