package factory.simplefactory.impl;

import factory.simplefactory.interfaces.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：PotatoSilk
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 15:31
 * 描述:
 * 版本：V1.0
 */
public class PotatoSilk implements Food {
    @Override
    public void prepare() {
        System.out.println("PotatoSilk>>>>>准备中ing");
    }

    @Override
    public void cook() {
        System.out.println("PotatoSilk>>>>>烹饪中ing");
    }

    @Override
    public void serve() {
        System.out.println("PotatoSilk>>>>>上菜ing");
    }

    @Override
    public void order() {
        prepare();
        cook();
        serve();

    }
}
