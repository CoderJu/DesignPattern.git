package factory.simplefactory.interfaces;/**
 * Created by User on 2019/1/12.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Food
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 15:26
 * 描述:食物抽象类
 * 版本：V1.0
 */
public interface Food {

    //准备
    void prepare();

    //烹饪
    void cook();

    //上菜
    void serve();

    //点菜
    void order();
}
