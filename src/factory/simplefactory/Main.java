package factory.simplefactory;

import factory.simplefactory.interfaces.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 15:34
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        Food food = MenuFactory.createFood(MenuFactory.MapoTofu_Type);
        food.order();
        System.out.println("-----------");
         food = MenuFactory.createFood(MenuFactory.PotatoSilk_Type);
        food.order();

    }
}
