package factory.simplefactory;/**
 * Created by User on 2019/1/12.
 */

import factory.simplefactory.impl.MapoTofu;
import factory.simplefactory.impl.PotatoSilk;
import factory.simplefactory.interfaces.Food;

/**
 * Created with IntelliJ IDEA.
 * 类名：MenuFactory
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 15:26
 * 描述: 菜单（抽象工厂）
 * 版本：V1.0
 */
public class MenuFactory {

    public static final int MapoTofu_Type = 1;
    public static final int PotatoSilk_Type = 2;

    //一般改方法都为静态方法
    public static Food createFood(int type){
        Food food = null;
        switch (type){
            case 1:
                food = new MapoTofu();
                break;
            case 2:
                food=new PotatoSilk();
                break;
                default:
                    System.out.println("请您先点菜！");
                    break;
        }
        return food;
    }

}
