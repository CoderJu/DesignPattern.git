package factory.absFactory;

import factory.absFactory.impl.factory.AndroidFactory;
import factory.absFactory.impl.factory.AppleFactory;
import factory.absFactory.intrefaces.factory.AbsFactory;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:17
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        AbsFactory factory = null;

        factory = new AndroidFactory();
        factory.productCpu().cup();
        factory.productDataWire().dataWire();

        System.out.println("=========================================");

        factory = new AppleFactory();
        factory.productCpu().cup();
        factory.productDataWire().dataWire();
    }
}
