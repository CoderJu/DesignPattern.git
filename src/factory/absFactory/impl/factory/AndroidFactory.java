package factory.absFactory.impl.factory;

import factory.absFactory.impl.product.AndroidCpu;
import factory.absFactory.impl.product.AndroidDataWire;
import factory.absFactory.intrefaces.factory.AbsFactory;
import factory.absFactory.intrefaces.product.Cpu;
import factory.absFactory.intrefaces.product.DataWire;

/**
 * Created with IntelliJ IDEA.
 * 类名：AndroidFactory
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:14
 * 描述:
 * 版本：V1.0
 */
public class AndroidFactory  implements AbsFactory {

    @Override
    public DataWire productDataWire() {
        return new AndroidDataWire();
    }

    @Override
    public Cpu productCpu() {
        return new AndroidCpu();
    }
}
