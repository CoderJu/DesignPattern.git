package factory.absFactory.impl.product;

import factory.absFactory.intrefaces.product.Cpu;

/**
 * Created with IntelliJ IDEA.
 * 类名：AppleCpu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:10
 * 描述:苹果CPu
 * 版本：V1.0
 */
public class AppleCpu implements Cpu {
    @Override
    public void cup() {
        System.out.println("---------------Apple Cpu------------");
    }
}
