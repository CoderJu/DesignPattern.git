package factory.absFactory.impl.product;

import factory.absFactory.intrefaces.product.DataWire;

/**
 * Created with IntelliJ IDEA.
 * 类名：AppleDataWire
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:08
 * 描述:苹果数据线
 * 版本：V1.0
 */
public class AppleDataWire  implements DataWire{
    @Override
    public void dataWire() {
        System.out.println(">>>>>>>>>>苹果数据线<<<<<<<<<");
    }
}
