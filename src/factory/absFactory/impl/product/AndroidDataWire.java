package factory.absFactory.impl.product;

import factory.absFactory.intrefaces.product.DataWire;

/**
 * Created with IntelliJ IDEA.
 * 类名：AndroidDataWire
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:09
 * 描述:安卓数据线
 * 版本：V1.0
 */
public class AndroidDataWire implements DataWire {
    @Override
    public void dataWire() {
        System.out.println(">>>>>>>>>安卓数据线<<<<<<<<<<<<<<<");
    }
}
