package factory.absFactory.impl.product;

import factory.absFactory.intrefaces.product.Cpu;

/**
 * Created with IntelliJ IDEA.
 * 类名：AndroidCpu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:11
 * 描述:安卓Cpu
 * 版本：V1.0
 */
public class AndroidCpu implements Cpu {
    @Override
    public void cup() {
        System.out.println("-----------Android Cpu--------");
    }
}
