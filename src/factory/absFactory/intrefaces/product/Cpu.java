package factory.absFactory.intrefaces.product;

/**
 * Created with IntelliJ IDEA.
 * 类名：Cpu
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:10
 * 描述:Cpu
 * 版本：V1.0
 */
public interface Cpu {
    void cup();
}
