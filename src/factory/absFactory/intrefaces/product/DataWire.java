package factory.absFactory.intrefaces.product;

/**
 * Created with IntelliJ IDEA.
 * 类名：DataWire
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:07
 * 描述:数据线产品
 * 版本：V1.0
 */
public interface DataWire {

    void dataWire();
}
