package factory.absFactory.intrefaces.factory;/**
 * Created by User on 2019/1/12.
 */

import factory.absFactory.intrefaces.product.Cpu;
import factory.absFactory.intrefaces.product.DataWire;

/**
 * Created with IntelliJ IDEA.
 * 类名：AbsFactory
 * 开发人员: CoderJu
 * 创建时间: 2019/1/12 21:11
 * 描述:
 * 版本：V1.0
 */
public interface AbsFactory {

    DataWire productDataWire();

    Cpu productCpu();
}
