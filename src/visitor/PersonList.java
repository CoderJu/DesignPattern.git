package visitor;

import visitor.interfaces.Award;
import visitor.interfaces.Person;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * 类名：PersonList
 * 开发人员: CoderJu
 * 创建时间: 2019/3/1 21:03
 * 描述:
 * 版本：V1.0
 */
public class PersonList {
    private ArrayList<Person> personArrayList = null;

    public PersonList() {
        personArrayList = new ArrayList<Person>();
    }

    public void addPerson(Person person){
        personArrayList.add(person);
    }

    //遍历访问 集合中的每一个对象
    public void accept(Award award) {
        for(Object obj : personArrayList){
            ((Person)obj).accept(award);
        }
    }
}
