package visitor.interfaces;

/**
 * Created with IntelliJ IDEA.
 * 类名：Person
 * 开发人员: CoderJu
 * 创建时间: 2019/3/1 20:51
 * 描述:抽象元素角色
 * 版本：V1.0
 */
public interface Person {

    void accept(Award award);

}
