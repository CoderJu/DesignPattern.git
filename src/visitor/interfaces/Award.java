package visitor.interfaces;

import visitor.impl.Student;
import visitor.impl.Teacher;

/**
 * Created with IntelliJ IDEA.
 * 类名：Award
 * 开发人员: CoderJu
 * 创建时间: 2019/3/1 20:51
 * 描述:
 * 版本：V1.0
 */
public interface Award {
        void visitorStudent(Student student);
        void vivitorTeacher(Teacher teacher);
}
