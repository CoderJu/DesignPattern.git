package visitor;

import visitor.impl.AwardCheckOne;
import visitor.impl.AwardCheckTwo;
import visitor.impl.Student;
import visitor.impl.Teacher;
import visitor.interfaces.Person;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/3/1 21:05
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        PersonList personList = new PersonList();
        Person p1,p2,p3,p4;
        p1 = new Student("张三",1,80);
        p2 = new Student("李四",3,100);
        p3 = new Teacher("王五",9,100);
        p4 = new Teacher("呜呜呜",11,100);
        personList.addPerson(p1);
        personList.addPerson(p2);
        personList.addPerson(p3);
        personList.addPerson(p4);
        personList.accept(new AwardCheckOne());

        System.out.println("---------------分界线--------------------");

        personList.accept(new AwardCheckTwo());
    }
}
