package visitor.impl;

import visitor.interfaces.Award;

/**
 * Created with IntelliJ IDEA.
 * 类名：AwardCheckOne
 * 开发人员: CoderJu
 * 创建时间: 2019/3/1 20:57
 * 描述:
 * 版本：V1.0
 */
public class AwardCheckOne implements Award {
    @Override
    public void visitorStudent(Student student) {
        int numOfPaper = student.getNumOfPaper();
        float score = student.getScore();
        String name = student.getName();
        if(numOfPaper>2){
            System.out.println("学生"+name+"可以评选科研奖！");
        }
    }

    @Override
    public void vivitorTeacher(Teacher teacher) {
            int numOfPaper = teacher.getNumOfPaper();
            float score = teacher.getScore();
            String name = teacher.getName();
        if(numOfPaper>10){
            System.out.println("老师"+name+"可以评选科研奖！");
        }
    }
}
