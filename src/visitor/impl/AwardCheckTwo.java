package visitor.impl;

import visitor.interfaces.Award;

/**
 * Created with IntelliJ IDEA.
 * 类名：AwardCheckTwo
 * 开发人员: CoderJu
 * 创建时间: 2019/3/1 21:00
 * 描述:
 * 版本：V1.0
 */
public class AwardCheckTwo implements Award {
    @Override
    public void visitorStudent(Student student) {
        int numOfPaper = student.getNumOfPaper();
        float score = student.getScore();
        String name = student.getName();
        if(score>=90){
            System.out.println("学生"+name+"可以评选成绩优秀奖!");
        }
    }

    @Override
    public void vivitorTeacher(Teacher teacher) {
        int numOfPaper = teacher.getNumOfPaper();
        float score = teacher.getScore();
        String name = teacher.getName();
        if(score>=90){
            System.out.println("老师"+name+"可以评选成绩优秀奖!");
        }
    }
}
