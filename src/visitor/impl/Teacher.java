package visitor.impl;

import visitor.interfaces.Award;
import visitor.interfaces.Person;

/**
 * Created with IntelliJ IDEA.
 * 类名：Teacher
 * 开发人员: CoderJu
 * 创建时间: 2019/3/1 20:55
 * 描述:
 * 版本：V1.0
 */
public class Teacher implements Person {

    private String name;
    private int numOfPaper;
    private float score;

    public Teacher(String name, int numOfPaper, float score) {
        this.name = name;
        this.numOfPaper = numOfPaper;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumOfPaper() {
        return numOfPaper;
    }

    public void setNumOfPaper(int numOfPaper) {
        this.numOfPaper = numOfPaper;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    @Override
    public void accept(Award award) {
            award.vivitorTeacher(this);
    }
}
