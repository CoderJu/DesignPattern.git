package builder;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * 类名：VacationDay
 * 开发人员: CoderJu
 * 创建时间: 2019/1/30 21:48
 * 描述:每天出游计划
 * 版本：V1.0
 */
public class VacationDay {
    private Date date;
    private String hotel;
    private ArrayList<String> events;
    private ArrayList<String> tickets;

    public VacationDay(Date dt) {
        date = dt;
        events = new ArrayList<String>();
        tickets = new ArrayList<String>();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String hotel) {
        this.hotel = hotel;
    }

    public void addEvents(String event){
        events.add(event);
    }

    public void addtickets(String ticket){
        tickets.add(ticket);
    }

    @Override
    public String toString() {
        return date + "VacationDay>>>>" +
                "date=" + date +
                ", hotel='" + hotel + '\'' +
                ", events=" + events +
                ", tickets=" + tickets ;
    }
}
