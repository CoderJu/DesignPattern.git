package builder;

import builder.interfaces.Builder;

/**
 * Created with IntelliJ IDEA.
 * 类名：Director
 * 开发人员: CoderJu
 * 创建时间: 2019/1/30 22:19
 * 描述:
 * 版本：V1.0
 */
public class Director {

    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public void setBuilder(Builder builder) {
        this.builder = builder;
    }

    public void construct(){
        builder.buildvacation();
        builder.getVacation().showInfo();
    }
}
