package builder;

import builder.interfaces.FourDaysBuilder;

/**
 * Created with IntelliJ IDEA.
 * 类名：Mian
 * 开发人员: CoderJu
 * 创建时间: 2019/1/30 22:22
 * 描述:
 * 版本：V1.0
 */
public class Main {



    public static void main(String[] args) {
        Director director = new Director(new FourDaysBuilder("2019-01-30"));
        director.construct();

    }
}
