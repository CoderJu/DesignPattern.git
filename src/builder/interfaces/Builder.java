package builder.interfaces;

import builder.Vacation;

/**
 * Created with IntelliJ IDEA.
 * 类名：Builder
 * 开发人员: CoderJu
 * 创建时间: 2019/1/30 22:08
 * 描述:抽象建造者角色
 * 版本：V1.0
 */
public interface Builder {

    void buildvacation();

    void buildDay(int i);

    void addHotel(String hotel);

    void addTicket(String ticket);

    void addEvent(String event);

    Vacation getVacation();
}
