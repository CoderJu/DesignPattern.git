package builder.interfaces;

import builder.Vacation;

/**
 * Created with IntelliJ IDEA.
 * 类名：FourDaysBuilder
 * 开发人员: CoderJu
 * 创建时间: 2019/1/30 22:10
 * 描述:
 * 版本：V1.0
 */
public class FourDaysBuilder implements Builder {

    private Vacation vacation;

    public FourDaysBuilder(String date) {
        vacation = new Vacation(date);
    }

    /**
     * 创建完整的假期的方法
     */
    @Override
    public void buildvacation() {

        addTicket("机票");
        addHotel("如家");
        addEvent("住宿");
        addEvent("晚餐");

        vacation.addDay();;
        addTicket("公园门票");
        addEvent("游城市公园");
        addTicket("水族馆门票");
        addEvent("游水族馆");
        addHotel("希尔顿酒店");

        vacation.addDay();
        addEvent("中央公园");
        addTicket("观光bus");
        addHotel("万达酒店");

        vacation.addDay();
        addTicket("机票");
        addEvent("回家");
    }

    @Override
    public void buildDay(int i) {
        vacation.setVacationDay(i);
    }

    @Override
    public void addHotel(String hotel) {
        vacation.setHotel(hotel);
    }

    @Override
    public void addTicket(String ticket) {
        vacation.setTicket(ticket);
    }

    @Override
    public void addEvent(String event) {
        vacation.setEvent(event);
    }

    @Override
    public Vacation getVacation() {
        return vacation;
    }


}
