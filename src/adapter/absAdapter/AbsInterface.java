package adapter.absAdapter;/**
 * Created by User on 2019/1/14.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：AbsInterface
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 22:24
 * 描述:
 * 版本：V1.0
 */
public interface AbsInterface {
    void method1();
    void method2();
    void method3();
    void method4();
}
