package adapter.absAdapter;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 22:29
 * 描述:
 * 版本：V1.0
 */
public class Main {

    public static void main(String[] args) {
        AbsInterface absInterface = new Target();
        absInterface.method3();
    }
}
