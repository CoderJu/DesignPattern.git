package adapter.absAdapter;

/**
 * Created with IntelliJ IDEA.
 * 类名：InterAdapter
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 22:25
 * 描述:
 * 版本：V1.0
 */
public  abstract class InterAdapter implements AbsInterface {
    @Override
    public void method1() {

    }

    @Override
    public void method2() {

    }

    @Override
    public void method3() {

    }

    @Override
    public void method4() {

    }
}
