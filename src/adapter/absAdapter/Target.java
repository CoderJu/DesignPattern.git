package adapter.absAdapter;

/**
 * Created with IntelliJ IDEA.
 * 类名：Adaptee
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 22:26
 * 描述:
 * 版本：V1.0
 */
public class Target  extends InterAdapter{

    @Override
    public void method3() {
        System.out.println(">>>>>>method3");
    }
}
