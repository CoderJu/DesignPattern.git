package adapter.classAdapter;


import adapter.classAdapter.interfaces.Charger;
import adapter.objAdapter.ObjInterfaceAdapter;

/**
 * Created with IntelliJ IDEA.
 * 类名：Main
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 21:52
 * 描述:
 * 版本：V1.0
 */
public class Main {
    public static void main(String[] args) {
        Charger charger = new InterfaceAdapter();
        charger.lightingInterface();
        charger.typeCInterface();

        Charger charger1 = new ObjInterfaceAdapter(new TypeC());
        charger1.typeCInterface();
        charger1.lightingInterface();
    }
}
