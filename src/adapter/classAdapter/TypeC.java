package adapter.classAdapter;

/**
 * Created with IntelliJ IDEA.
 * 类名：TypeC
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 21:48
 * 描述: 被装饰对象 TypeC
 * 版本：V1.0
 */
public class TypeC {
    public void typeCInterface(){
        System.out.println("我只有TypeC接口....");
    }
}
