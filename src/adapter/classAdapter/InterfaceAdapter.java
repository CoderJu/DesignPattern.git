package adapter.classAdapter;

import adapter.classAdapter.interfaces.Charger;

/**
 * Created with IntelliJ IDEA.
 * 类名：InterfaceAdapter
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 21:50
 * 描述:
 * 版本：V1.0
 */
public class InterfaceAdapter extends TypeC implements Charger {
    @Override
    public void lightingInterface() {
        System.out.println("我有Lighting接口");
    }
}
