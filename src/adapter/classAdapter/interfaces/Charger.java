package adapter.classAdapter.interfaces;/**
 * Created by User on 2019/1/14.
 */

/**
 * Created with IntelliJ IDEA.
 * 类名：Charger
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 21:46
 * 描述:目标角色 充电器
 * 版本：V1.0
 */
public interface Charger {
    //typeC接口
    void typeCInterface();
    //lighting接口
    void lightingInterface();
}
