package adapter.objAdapter;

import adapter.classAdapter.TypeC;
import adapter.classAdapter.interfaces.Charger;

/**
 * Created with IntelliJ IDEA.
 * 类名：ObjInterfaceAdapter
 * 开发人员: CoderJu
 * 创建时间: 2019/1/14 21:58
 * 描述:对象适配器
 * 版本：V1.0
 */
public class ObjInterfaceAdapter implements Charger {
    private TypeC typeC;
    public ObjInterfaceAdapter(TypeC typeC) {
        this.typeC = typeC;
    }
    @Override
    public void typeCInterface() {
        typeC.typeCInterface();
    }
    @Override
    public void lightingInterface() {
        System.out.println("我有Lighting接口");
    }
}
