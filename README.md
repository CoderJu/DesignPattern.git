# DesignPattern

#### 介绍
本代码示例用于学习JAVA23种设计模式

#### 软件架构
无

#### 对应包路径介绍
1. 策略模式示例代码：DesignPattern/src/strategy
2. 观察者模式示例代码：DesignPattern/src/observer
3. 装饰者模式示例代码: DesignPattern/src/decorator
4. 单例模式示例代码：DesignPattern/src/singleton
5. 工厂模式

    5.1.简单工厂模式示例代码：DesignPattern/src/factory/simplefactory
    
    5.2.工厂方法模式示例代码：DesignPattern/src/factory/factoryMethod
    
    5.3.抽象工厂模式示例代码：DesignPattern/src/factory/absfactory
6. 命令模式示例代码：DesignPattern/src/command
7. 适配器模式示例代码：DesignPattern/src/adapter
8. 外观模式示例代码：DesignPattern/src/facade
9. 模板模式示例代码：DesignPattern/src/template
10. 迭代器模式示例代码：DesignPattern/src/iterator
11. 组合模式示例代码：DesignPattern/src/composite
12. 状态模式示例代码：DesignPattern/src/state
13. 代理模式示例代码 DesignPattern/src/proxy
14. 桥接模式示例代码 DesignPattern/src/bridge
15. 生成器模式示例代码 DesignPattern/src/builder
16. 责任链模式示例代码 DesignPattern/src/chainms
17. 享元模式示例代码 DesignPattern/src/flyWeight
18. 解释器模式示例代码 DesignPattern/src/interpreter
19. 中介者模式示例代码 DesignPattern/src/mediator
20. 备忘录模式示例代码 DesignPattern/src/memento
21. 原型模式示例代码 DesignPattern/src/prototype
22. 访问者模式示例代码 DesignPattern/src/visitor
#### 模式介绍

1. [JAVA23种设计模式之策略模式](https://blog.csdn.net/system_obj/article/details/86232080)
2. [JAVA23种设计模式之观察者模式](https://blog.csdn.net/system_obj/article/details/86323810)
3. [JAVA23种设计模式之装饰者模式](https://blog.csdn.net/system_obj/article/details/86378072)
4. [JAVA23种设计模式之单例模式](https://blog.csdn.net/system_obj/article/details/86441347)
5. 工厂模式

    5.1. [JAVA23种设计模式之工厂模式(一)简单工厂模式](https://blog.csdn.net/system_obj/article/details/86500920)
    
    5.2. [JAVA23种设计模式之工厂模式(二)工厂方法模式](https://blog.csdn.net/system_obj/article/details/86516277)
    
    5.3. [JAVA23种设计模式之工厂模式(三)抽象工厂模式](https://blog.csdn.net/system_obj/article/details/86586328)
    
6. [JAVA23种设计模式之迭代器模式](https://blog.csdn.net/system_obj/article/details/86619392)
7. [JAVA23种设计模式之组合模式](https://blog.csdn.net/system_obj/article/details/86671259)
8. [JAVA23种设计模式之状态模式](https://blog.csdn.net/system_obj/article/details/86709646)
9. 代理模式

    9.1. [JAVA23种设计模式之代理模式(一)远程代理RMI](https://blog.csdn.net/system_obj/article/details/87869592)
    
    9.2. [JAVA23种设计模式之代理模式(二)静态代理模式](https://blog.csdn.net/system_obj/article/details/87909288)
    
    9.3. [JAVA23种设计模式之代理模式(二)动态代理模式](https://blog.csdn.net/system_obj/article/details/87927080)

10. [JAVA23种设计模式之命令模式](https://blog.csdn.net/system_obj/article/details/87958347)
11. [JAVA23种设计模式之组合模式](https://blog.csdn.net/system_obj/article/details/87998484)
12. [JAVA23种设计模式之复合模式](https://blog.csdn.net/system_obj/article/details/88070153)
13. [JAVA23种设计模式之桥接模式](https://blog.csdn.net/system_obj/article/details/88138832)
14. [JAVA23种设计模式之生成器模式](https://blog.csdn.net/system_obj/article/details/88213310)
15. [JAVA23种设计模式之责任链模式](https://blog.csdn.net/system_obj/article/details/88323873)
16. [JAVA23种设计模式之享元模式(蝇量模式)](https://blog.csdn.net/system_obj/article/details/88360660)
17. [JAVA23种设计模式之解释器模式](https://blog.csdn.net/system_obj/article/details/88383147)
18. [JAVA23种设计模式之中介者模式](https://blog.csdn.net/system_obj/article/details/88412735)
19. [JAVA23种设计模式之备忘录模式](https://blog.csdn.net/system_obj/article/details/88431996)
20. [JAVA23种设计模式之原型模式](https://blog.csdn.net/system_obj/article/details/88564211)
21. [JAVA23种设计模式之访问者模式](https://blog.csdn.net/system_obj/article/details/88587101)
22. [JAVA23种设计模式之外观模式](https://blog.csdn.net/system_obj/article/details/88630080)
23. [JAVA23种设计模式之模板模式](https://blog.csdn.net/system_obj/article/details/88677690)
24. [JAVA23种设计模式总结](https://blog.csdn.net/system_obj/article/details/88703454)
#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

